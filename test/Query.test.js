import expect from 'expect';

import Query from '../lib/Query';

describe('Query', () => {

  it('create without error', () => {
    new Query();
  });

  it('#push', () => {
    const query = new Query();
    query.push(['a', 'a', 'b']);

    expect(query.size()).toEqual(2);
  });

  it('items', () => {
    const query = new Query();
    expect(query.isEmpty()).toExist();
    expect(query.size()).toEqual(0);

    query.push([{ instance: 'a' }]);

    expect(query.isEmpty()).toNotExist();
    expect(query.size()).toEqual(1);
    expect(query.first()).toBe('a');
    expect(query.last()).toBe('a');
  });

  it('#all', () => {
    const query = new Query();
    query.push([{ instance: 'a' }, { instance: 'b' }, { instance: 'c' }]);

    expect(query.all()).toEqual(['a', 'b', 'c']);
    
    const match = [];
    query.all(val => match.push(val));
    expect(match).toEqual(['a', 'b', 'c']);
  });

});
