import expect from 'expect';

import Position from '../lib/Position';

describe('Position', () => {

  it('creates without errors', () => {
    const pos = new Position();

    expect(pos.next).toEqual(null);
    expect(pos.previous).toEqual(null);
    expect(pos.instance).toEqual(null);
    expect(pos.fieldIndex).toEqual({});
  });

  it('.setIndex - sets index for a field', () => {
    const pos = new Position();

    pos.setIndex('a', 9);

    expect(pos.fieldIndex).toEqual({ a: 9});
  });

  it('.getIndex - returns index for a field', () => {
    const pos = new Position();

    pos.setIndex('a', 9);

    expect(pos.getIndex('a')).toEqual(9);
  });

  it('.unsetIndex - removes index of a field', () => {
    const pos = new Position();

    pos.setIndex('a', 9);

    expect(pos.unsetIndex('a')).toEqual(undefined);
  });

});
