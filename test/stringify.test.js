import expect from 'expect';

import stringify from '../lib/stringify';

describe('stringify', () => {

  it('string', () => {
    expect(stringify('')).toEqual('');
    expect(stringify('a')).toEqual('a');
  });

  it('number', () => {
    expect(stringify(0)).toEqual('0');
  });

  it('null', () => {
    expect(stringify(null)).toEqual('null');
  });

  it('undefined', () => {
    expect(stringify()).toEqual('undefined');
  });

  it('boolean', () => {
    expect(stringify(true)).toEqual('true');
    expect(stringify(false)).toEqual('false');
  });

  it('array', () => {
    expect(stringify([{a:1},{b:null}])).toEqual('[{"a":1},{"b":null}]');
  });

  it('object', () => {
    expect(stringify({a:1})).toEqual('{"a":1}');
  });

});
