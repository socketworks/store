import expect from 'expect';

import Instance from '../lib/Instance';
import Resource from '../lib/Resource';
import Namespace from '../lib/Namespace';

import store, { namespace, resource } from '../lib/main';

describe('main', () => {

  it('exposes public modules/classes', () => {
    expect(store.Namespace).toEqual(Namespace);
    expect(store.Resource).toEqual(Resource);
    expect(store.Instance).toEqual(Instance);
  });

  it('create namespace within the module', () => {
    expect(namespace).toBeA(Function);
    expect(namespace('a')).toBeA(Namespace);
  });

  it('create unmanaged resource outside of the module', () => {
    expect(resource).toBeA(Function);
    expect(resource('a')).toBeA(Resource);
  });

  it('events', () => {
    expect(store.events).toEqual({
      CREATE: 'create',
      CHANGE: 'change',
      UPDATE: 'update',
      REMOVE: 'remove',
      RESET: 'reset'
    });
  });

});
