import expect from 'expect';

import { CREATE, CHANGE, UPDATE, REMOVE, RESET } from '../lib/Evented';
import Instance from '../lib/Instance';
import Resource from '../lib/Resource';

describe('Resource', () => {

  it('create without error', () => {
    const res = new Resource();

    expect(res.name).toEqual('');
    expect(res.idAttribute).toEqual('id');
    expect(res.namespace).toEqual(null);
    expect(Object.keys(res.listeners)).toEqual([CREATE, CHANGE, UPDATE, REMOVE, RESET]);
    expect(res._index.indexKeys).toInclude('id');
  });

  it('create with empty idAttribute', () => {
    const res = new Resource('', '');

    expect(res.name).toEqual('');
    expect(res.idAttribute).toEqual('');
    expect(res.namespace).toEqual(null);
    expect(res._index.indexKeys).toExclude('id');
    expect(res._index.indexKeys.length).toEqual(0);
  });

  it('_create', () => {
    const res = new Resource();
    const listener = expect.createSpy();
    res.subscribe(CREATE, listener);
    res.store({ id: 1 });

    expect(res.order.length).toEqual(1);
    expect(listener).toHaveBeenCalled();
  });

  it('subscribe to Instance updates', () => {
    const res = new Resource();
    const instance = res.store({ id: 1 });
    const listener = expect.createSpy();
    instance.subscribe(UPDATE, listener);

    instance.update({ name: 'a' });

    expect(listener).toHaveBeenCalled();
    expect(listener.calls[0].arguments[0]).toEqual(instance);
    expect(listener.calls[0].arguments[1]).toEqual({ id: 1 });
    expect(listener.calls[0].arguments[2]).toEqual({ name: 'a' });
  });

  it('#find, #last, #at, #size', () => {
    const res = new Resource();
    res.store({ id: 1 });
    res.store({ id: 2 });
    res.store({ id: 3 });

    expect(res.first().getProps()).toEqual({ id: 1 });
    expect(res.at(1).getProps()).toEqual({ id: 2 });
    expect(res.last().getProps()).toEqual({ id: 3 });
    expect(res.size()).toEqual(3);
  });

  it('#index', () => {
    const res = new Resource();
    res.index('a', 'b', 'c');

    expect(res._index.indexKeys).toEqual(['id', 'a', 'b', 'c'])
  });

  it('#body', () => {
    const res = new Resource();
    res.body('body');
    expect(res.body()).toEqual('body');
  });

  it('#set, #get', () => {
    const res = new Resource();

    res.set('a', 1);

    expect(res.get('a')).toEqual(1);
  });

  it('#change', () => {
    const res = new Resource();
    const changeSpy = expect.spyOn(res, 'publish');

    res.change({ a: 1, b: 2 });

    expect(res.get('a')).toEqual(1);
    expect(res.get('b')).toEqual(2);
    expect(changeSpy).toHaveBeenCalled();
    expect(changeSpy.calls[0].arguments[0]).toEqual(CHANGE);
    expect(changeSpy.calls[0].arguments.length).toEqual(2);
    expect(changeSpy.calls[0].arguments[1]).toEqual({ a: 1, b: 2});

    changeSpy.restore();
  });

  it('#store - creates ordered chain of Positions', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, a: 1 });

    expect(instance1.previous()).toNotExist();
    expect(instance1.next()).toEqual(instance2);
    expect(instance2.previous()).toEqual(instance1);
    expect(instance2.next()).toEqual(instance3);
    expect(instance3.previous()).toEqual(instance2);
    expect(instance3.next()).toNotExist();

  });

  it('#store - inserts instance at the specified position', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instance3 = res.store({ id: 2, a: 1 });

    const instance2 = res.store({ id: 1, a: 1 }, 1);

    expect(instance1.previous()).toNotExist();
    expect(instance1.next()).toEqual(instance2);
    expect(instance2.previous()).toEqual(instance1);
    expect(instance2.next()).toEqual(instance3);
    expect(instance3.previous()).toEqual(instance2);
    expect(instance3.next()).toNotExist();

  });

  it('#store - inserts multiple instances', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instances = res.store([
      { id: 1, a: 1 },
      { id: 2, a: 1 }
    ]);

    expect(instance1.previous()).toNotExist();
    expect(instance1.next()).toEqual(instances[0]);
    expect(instances[0].previous()).toEqual(instance1);
    expect(instances[0].next()).toEqual(instances[1]);
    expect(instances[1].previous()).toEqual(instances[0]);
    expect(instances[1].next()).toNotExist();
  });

  it('#store - inserts multiple instances after the sepcified position', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, a: 1 });

    const instances = res.store([
      { id: 3, a: 1 },
      { id: 4, a: 1 }
    ], 2);

    expect(res.at(1)).toEqual(instance2);
    expect(res.at(2)).toEqual(instances[0]);
    expect(res.at(3)).toEqual(instances[1]);
    expect(res.at(4)).toEqual(instance3);
  });

  it('#all - maps up the instances or executes the iterator', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, a: 1 });

    expect(res.all()).toEqual([instance1, instance2, instance3]);
    expect(res.all(instance => instance.get('id'))).toEqual([0, 1, 2]);
  });

  it('#find - indexed', () => {
    const res = new Resource();
    res.index('a');
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 2 });

    expect(res.find('id', 0).first()).toEqual(instance1);
    expect(res.find('a', 2).first()).toEqual(instance2);
  });

  it('#find - unindexed', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 2 });
    const instance3 = res.store({ id: 2, a: 3 });

    expect(res.find('a', 3).first()).toEqual(instance3);
  });

  it('#store - creates an instance', () => {
    const res = new Resource();
    res.index('a', 'b');

    const instance = res.store({ id: 0, a: 1, b: 2 });

    expect(res.find('id', 0).first()).toEqual(instance);
  });

  it('#remove', () => {
    const res = new Resource();
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, a: 1 });

    res.remove(instance2);

    expect(instance1.previous()).toNotExist();
    expect(instance1.next()).toEqual(instance3);
    expect(instance3.previous()).toEqual(instance1);
    expect(instance3.next()).toNotExist();

    expect(res._index.lookup.id[1]).toNotExist();
  });

  it('#query', () => {
    const res = new Resource();
    res.index('a', 'b');
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, b: 2 });

    const query = res.query({ a: 1, b: 2 });

    expect(query.size()).toEqual(3);
    expect(query.first().get('id')).toEqual(0);
    expect(query.at(1).get('id')).toEqual(1);
    expect(query.last().get('b')).toEqual(2);
  });

  it('#position', () => {
    const res = new Resource();
    res.index('a', 'b');
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, b: 2 });

    const positions = res.position('id', 1);

    expect(positions).toEqual([1]);
  });

  it('#before', () => {
    const res = new Resource();
    res.index('a');
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, b: 2 });
    const instance4 = res.before(instance2, { id: 3, a: 1});

    expect(res.at(1)).toEqual(instance4);
  });

  it('#after', () => {
    const res = new Resource();
    res.index('a');
    const instance1 = res.store({ id: 0, a: 1 });
    const instance2 = res.store({ id: 1, a: 1 });
    const instance3 = res.store({ id: 2, b: 2 });
    const instance4 = res.after(instance2, { id: 3, a: 1});

    expect(res.at(2)).toEqual(instance4);
  });

  it('#values', () => {
    const res = new Resource();
    res.index('a');

    let i = 0;
    for (; i < 100; i += 1) {
      res.store({
        id: i,
        a: i < 33 ? 'a' : i < 66 ? 'b' : 'c'
      });
    }

    expect(res.values('a')).toEqual(['a', 'b', 'c']);
  });

  it('"create" event fires when an instance is created', () => {
    const res = new Resource();
    res.index('a');

    let payload = null;
    let instance = null;

    res.subscribe("create", (data, index) => {
      payload = [data, index];
    });

    instance = res.store({ id: 0, a: 'a' });

    expect(payload).toEqual([instance, 0]);
  });

  it('"create" event fires when a list of instances is created', () => {
    const res = new Resource();
    res.index('a');

    let payload = [];
    let instances = null;

    res.subscribe("create", (data, index) => {
      payload.push([data, index]);
    });

    instances = res.store([
      { id: 0, a: 'a' },
      { id: 1, a: 'a' },
      { id: 2, a: 'a' }
    ]);

    expect(payload.length).toEqual(3);
    payload.forEach((data, i) => {
      expect(data).toEqual([instances[i], i]);
    });
  });

  it('"update" event fires when an instance is updated', (done) => {
    const res = new Resource();
    res.index('a');
    const a = res.store({ id: 0, a: 'a' });

    res.subscribe("update", (i, oldProps, delta) => {
      expect(oldProps).toEqual({ id: 0, a: 'a' });
      expect(delta).toEqual({ a: 'b' });

      done();
    });

    a.update({ a: 'b' });
  });

});
