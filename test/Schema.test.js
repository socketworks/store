import expect from 'expect';
import Schema from '../lib/Schema';

describe('Schema', () => {

  it('.defineValue - function > creates an instance', () => {
    expect(Schema.defineValue(Array)).toBeAn(Array);
  });

  it('.defineValue - object > clones', () => {
    const obj = { a: {} };
    expect(Schema.defineValue(obj)).toEqual(obj);
    expect(Schema.defineValue(obj)).toNotBe(obj);
    expect(Schema.defineValue(obj.a)).toNotBe(obj.a);
  });

  it('.defineValue - array > clones', () => {
    const obj = [{ a: {} }];
    expect(Schema.defineValue(obj)).toEqual(obj);
    expect(Schema.defineValue(obj)).toNotBe(obj);
    expect(Schema.defineValue(obj[0])).toNotBe(obj[0]);
  });

  it('.defineValue - default > returns', () => {
    expect(Schema.defineValue(null)).toEqual(null);
    expect(Schema.defineValue('a')).toEqual('a');
    expect(Schema.defineValue(1)).toEqual(1);
  });

  it('.get - defaults values', () => {
    const props = {
      a: 'a',
      b: Array,
      c: {
        name: 'bla'
      },
      d: [{}]
    };
    
    const parsed = Schema.get(props);

    expect(parsed).toEqual({
      a: 'a',
      b: [],
      c: {
        name: 'bla'
      },
      d: [{}]
    });
    expect(parsed.a).toBe(props.a);
    expect(parsed.b).toBeAn(props.b);
    expect(parsed.c).toNotBe(props.c);
    expect(parsed.d).toNotBe(props.d);
  });
  
});
