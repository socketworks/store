import expect from 'expect';

import Index from '../lib/Index';
import Position from '../lib/Position';

describe('Index', () => {

  it('can create without errors', () => {
    const index = new Index();

    expect(index.lookup).toExist();
    expect(index.indexKeys).toExist();
    expect(index.lookup).toBeAn(Object);
    expect(index.indexKeys).toBeAn(Array);
  });

  it('#indexKey', () => {
    const index = new Index();

    index.indexKey('a');
    index.indexKey('b');
    index.indexKey('b');

    expect(index.indexKeys.length).toEqual(2);
    expect(index.indexKeys).toInclude('a');
    expect(index.indexKeys).toInclude('b');
  });

  it('#indexPosition', () => {
    const value = 1;
    const index = new Index();
    const pos = new Position();
    const setIndexSpy = expect.spyOn(pos, 'setIndex');
    index.indexKey('a');
    pos.instance = {
      getProps: () => ({ a: value })
    };

    index.indexPosition(pos);

    expect(index.lookup.a).toExist();
    expect(index.lookup.a[value]).toExist();
    expect(index.lookup.a[value]).toBeAn(Array);
    expect(index.lookup.a[value].length).toEqual(1);
    expect(index.lookup.a[value][0]).toExist(pos);

    expect(setIndexSpy).toHaveBeenCalled();
    // value is stringified
    expect(setIndexSpy).toHaveBeenCalledWith(value + '', 0);
  });

  it('#indexPosition - multiple', () => {
    const value = 1;
    const index = new Index();
    const pos1 = new Position();
    const pos2 = new Position();
    index.indexKey('a');
    pos1.instance = {
      getProps: () => ({ a: value })
    };
    pos2.instance = {
      getProps: () => ({ a: value })
    };

    index.indexPosition(pos1);
    index.indexPosition(pos2);

    expect(index.lookup.a[value].length).toEqual(2);
  });

  it('#removePosition', () => {
    const value = 1;
    const index = new Index();
    const pos = new Position();
    index.indexKey('a');
    pos.instance = {
      getProps: () => ({ a: value })
    };
    index.indexPosition(pos);

    index.removePosition(pos);

    expect(index.lookup.a[value]).toNotExist();
  });

  it('#removePosition - multiple', () => {
    const value = 1;
    const index = new Index();
    const pos1 = new Position();
    const pos2 = new Position();
    index.indexKey('a');
    pos1.instance = {
      getProps: () => ({ a: value })
    };
    pos2.instance = {
      getProps: () => ({ a: value })
    };
    index.indexPosition(pos1);
    index.indexPosition(pos2);

    index.removePosition(pos1);

    expect(index.lookup.a[value].length).toEqual(1);
    expect(index.lookup.a[value][0]).toEqual(pos2);
  });

  it('#reindexPosition', () => {
    const value = 1;
    const newValue = 2;
    const index = new Index();
    const pos = new Position();
    index.indexKey('a');
    pos.instance = {
      getProps: () => ({ a: value })
    };
    index.indexPosition(pos);
    index.reindexPosition(pos, { a: value }, { a: newValue });

    expect(index.lookup.a[value]).toNotExist();
    expect(index.lookup.a[newValue]).toBeAn(Array);
    expect(index.lookup.a[newValue].length).toEqual(1);
    expect(index.lookup.a[newValue][0]).toEqual(pos);
  });

  it('#get', () => {
    const value = 1;
    const index = new Index();
    const pos1 = new Position();
    const pos2 = new Position();
    index.indexKey('a');
    pos1.instance = {
      getProps: () => ({ a: value })
    };
    pos2.instance = {
      getProps: () => ({ a: value })
    };
    index.indexPosition(pos1);
    index.indexPosition(pos2);

    expect(index.get('a', value)).toBeAn(Array);
    expect(index.get('a', value).length).toEqual(2);
    expect(index.get('a', value)[0]).toEqual(pos1);
    expect(index.get('a', value)[1]).toEqual(pos2);
  });

  it('#getValuesForKey', () => {
    const index = new Index();
    const pos1 = new Position();
    const pos2 = new Position();
    index.indexKey('a');
    pos1.instance = {
      getProps: () => ({ a: 'hakuna' })
    };
    pos2.instance = {
      getProps: () => ({ a: 'matata' })
    };
    index.indexPosition(pos1);
    index.indexPosition(pos2);

    expect(index.getValuesFor('a')).toBeAn(Array);
    expect(index.getValuesFor('a')).toEqual(['hakuna', 'matata']);
  });

});
