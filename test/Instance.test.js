import expect from 'expect';

import { CHANGE, UPDATE } from '../lib/Evented';
import Instance from '../lib/Instance';

describe('Instance', () => {

  it('create without an error', () => {
    new Instance();
  });

  it('#id', () => {
    const resource = {
      idAttribute: 'id',
      body: () => 'body'
    };
    const props = {
      body: {
        id: 1
      }
    };
    const instance = new Instance(resource, props);

    expect(instance.id()).toEqual(1);
  });

  it('#get - with Resource#resourceBody', () => {
    const resource = {
      idAttribute: 'id',
      body: () => 'body'
    };
    const props = {
      body: {
        id: 1,
        name: 'bob'
      }
    };
    const instance = new Instance(resource, props);

    expect(instance.get('name')).toEqual('bob');
  });

  it('#get - without Resource#resourceBody', () => {
    const resource = {
      idAttribute: 'id',
      body: () => undefined
    };
    const props = {
      id: 1,
      name: 'bob'
    };
    const instance = new Instance(resource, props);

    expect(instance.get('name')).toEqual('bob');
  });

  it('#resolve', () => {
    const props = {
      a: {
        b: {
          c: 'c'
        }
      }
    };
    const instance = new Instance(null, props);

    expect(instance.resolve()).toEqual(props);
    expect(instance.resolve('a')).toEqual(props.a);
    expect(instance.resolve('a.b.c')).toEqual('c');
  });

  it('#getProps - with Resource#resourceBody', () => {
    const resource = {
      idAttribute: 'id',
      body: () => 'body'
    };
    const props = {
      body: {
        id: 1,
        name: 'bob'
      }
    };
    const instance = new Instance(resource, props);

    expect(instance.getProps()).toEqual({
      id: 1,
      name: 'bob'
    });
  });

  it('#getProps - without Resource#resourceBody', () => {
    const resource = {
      idAttribute: 'id',
      body: () => undefined
    };
    const props = {
      id: 1,
      name: 'bob'
    };
    const instance = new Instance(resource, props);

    expect(instance.getProps()).toEqual(props);
  });

  it('#toString', () => {
    const props = {
      a: {
        b: {
          c: 'c'
        }
      }
    };
    const instance = new Instance(null, props);

    expect(instance.toString()).toEqual(JSON.stringify(props));
  });

  it('#change', () => {
    const resource = {
      // idAttribute: 'id',
      body: () => undefined
    };
    const instance = new Instance(resource, {});

    instance.change({ a: 5 });

    expect(instance.get('a')).toEqual(undefined);
    expect(instance.getChange('a')).toEqual(5);
  });

  it('#change - fires "change" event', (done) => {
    const resource = {
      body: () => undefined
    };
    const instance = new Instance(resource, {});

    instance.subscribe(CHANGE, (i, oldChanges, changes) => {
      expect(oldChanges).toEqual({});
      expect(changes).toEqual({ a: 5 });

      done();
    });

    instance.change({ a: 5 });
  });

  it('#update - fires "update" event', (done) => {
    const resource = {
      // idAttribute: 'id',
      body: () => undefined
    };
    const instance = new Instance(resource, { a: 1, d: 6 });

    instance.subscribe(UPDATE, (i, oldProps, delta) => {
      expect(oldProps).toEqual({ a: 1, d: 6 });
      expect(delta).toEqual({ a: 3, b: 3, c: 5 });
      expect(i.getProps()).toEqual({ a: 3, b: 3, c: 5, d: 6 });

      done();
    });

    instance.change({ a: 2, b: 3 });
    instance.update({ a: 3, c: 5 });
  });

});
