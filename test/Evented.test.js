import expect from 'expect';
import Evented, { CREATE, CHANGE, UPDATE, REMOVE, RESET } from '../lib/Evented';

describe('Evented', () => {

  it('event constants', () => {
    expect(CREATE).toEqual('create');
    expect(CHANGE).toEqual('change');
    expect(UPDATE).toEqual('update');
    expect(REMOVE).toEqual('remove');
    expect(RESET).toEqual('reset');
  });

  it('create without errors', () => {
    const evented = new Evented();
    expect(evented.listeners).toBeAn('object');
  });

  it('#registerEvent - creates an event space for the listeners', () => {
    const evented = new Evented();

    evented.registerEvent('a');

    expect(evented.listeners.a).toBeAn(Array);
  });

  it('#registerEvents - creates multiple event spaces for the listeners', () => {
    const evented = new Evented();

    evented.registerEvents('a', 'b', 'c');

    expect(evented.listeners.a).toBeAn(Array);
    expect(evented.listeners.b).toBeAn(Array);
    expect(evented.listeners.c).toBeAn(Array);
  });

  it('#subscribe - only registers functions as listeners', () => {
    const evented = new Evented();
    const listener1 = () => {};
    const listener2 = () => {};
    evented.registerEvents('a');

    evented.subscribe('a', 5);
    evented.subscribe('a', listener1);
    evented.subscribe('a', listener2);
    evented.subscribe('b', () => {});

    expect(evented.listeners.a.length).toEqual(2);
    expect(evented.listeners.a[0]).toEqual(listener1);
    expect(evented.listeners.a[1]).toEqual(listener2);
    expect(evented.listeners.b).toNotExist();
  });

  it('#unsubscribe - removes subscribed listeners', () => {
    const evented = new Evented();
    const listener1 = () => {};
    const listener2 = () => {};
    evented.registerEvents('a');
    evented.subscribe('a', listener1);
    evented.subscribe('a', listener2);

    evented.unsubscribe('a', listener1);

    expect(evented.listeners.a.length).toEqual(1);
    expect(evented.listeners.a[0]).toEqual(listener2);
  });

  it('#publish - triggers subscribers synchronously with all arguments', () => {
    const evented = new Evented();
    const listener = expect.createSpy();
    evented.registerEvent('a');
    evented.subscribe('a', listener);

    evented.publish('a', 1, 2, 3);

    expect(listener).toHaveBeenCalled();
    expect(listener).toHaveBeenCalledWith(1, 2, 3);
  });

  it('#asyncPublish - triggers subscribers asynchronously (next execution frame) with all arguments', (done) => {
    const evented = new Evented();
    const listener = (...args) => {
      expect(args).toInclude(1);
      expect(args).toInclude(2);
      expect(args).toInclude(3);

      done();
    };
    evented.registerEvent('a');
    evented.subscribe('a', listener);

    evented.asyncPublish('a', 1, 2, 3);
  });

  it('#resetEvents - removes all listeners but preserves registered event spaces', () => {
    const evented = new Evented();
    const listener = () => {};
    evented.registerEvents('a', 'b', 'c');
    evented.subscribe('a', listener);
    evented.subscribe('b', listener);
    evented.subscribe('c', listener);

    evented.resetEvents();

    expect(evented.listeners.a).toBeAn(Array);
    expect(evented.listeners.b).toBeAn(Array);
    expect(evented.listeners.c).toBeAn(Array);
    expect(evented.listeners.a.length).toEqual(0);
    expect(evented.listeners.b.length).toEqual(0);
    expect(evented.listeners.c.length).toEqual(0);
  });

});
