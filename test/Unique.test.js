import expect from 'expect';

import Unique from '../lib/Unique';

describe('Unique', () => {

  it('instance', () => {
    const index = new Unique();
    expect(index.getId()).toMatch(/_\d+/);
  });

  it('id', () => {
    const id = Unique.id('test');
    expect(id).toMatch(/test\d+/);
  });

});
