import expect from 'expect';

import Indexable from '../lib/Indexable';

describe('Indexable', () => {

  it('get/set positionId', () => {
    const index = new Indexable();
    index.setPositionId('a');

    expect(index.getPositionId()).toEqual('a');
  });

  it('ensure', () => {
    const indexable = new Indexable();

    expect(Indexable.ensure(indexable)).toBe(undefined);
    expect(() => Indexable.ensure({})).toThrow(TypeError, 'object is not an Indexable');
  });

});
