import expect from 'expect';
import Resource from '../lib/Resource';
import Namespace, { NAMESPACES } from '../lib/Namespace';

class XResource extends Resource {
  constructor() {
    super(...arguments);
    this.a = 'a';
  }
}

describe('Namespace', () => {

  afterEach(() => {
    Object.keys(NAMESPACES).forEach(name => delete NAMESPACES[name]);
  });
  
  it('cannot create namespace without a name', () => {
    expect(() => new Namespace()).toThrow('[name] must be a valid string!');
    expect(NAMESPACES).toEqual({});
  });

  it('.create - create namespace with a name', () => {
    const ns = Namespace.create('test');
    const ns1 = new Namespace('test');

    expect(ns.name).toEqual('test');
    expect(ns.resources).toEqual({});
    expect(NAMESPACES['test']).toEqual(ns);
    expect(ns).toEqual(ns1);
  });

  it('#resource - creates and registers a named resource', () => {
    const ns = Namespace.create('test');
    const resource = ns.resource('user');

    expect(resource).toBeA(Resource);
    expect(ns.resource('user')).toEqual(resource);
  });

  it('#resource - creates a custom resources if specified', () => {
    const ns = Namespace.create('test');
    const resource = ns.resource('user', undefined, XResource);

    expect(resource).toBeA(XResource);
    expect(resource.a).toEqual('a');
  });

  it('creates Resource with Namespace defaults', () => {
    const ns = Namespace.create('test');
    ns.id('_id');
    ns.index('a', 'b', 'c');
    ns.body('body');

    const resource = ns.resource('test');
    expect(resource.idAttribute).toEqual('_id');
    expect(resource.resourceBody).toEqual('body');
    expect(resource._index.indexKeys).toInclude('a');
    expect(resource._index.indexKeys).toInclude('b');
    expect(resource._index.indexKeys).toInclude('c');
  });

});
