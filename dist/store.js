(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["sktwrx_store"] = factory();
	else
		root["sktwrx_store"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.resource = exports.namespace = undefined;
	
	var _Instance = __webpack_require__(1);
	
	var _Instance2 = _interopRequireDefault(_Instance);
	
	var _Resource = __webpack_require__(3);
	
	var _Resource2 = _interopRequireDefault(_Resource);
	
	var _Namespace = __webpack_require__(9);
	
	var _Namespace2 = _interopRequireDefault(_Namespace);
	
	var _Evented = __webpack_require__(2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var namespace = exports.namespace = function namespace(_namespace) {
	  return _Namespace2.default.create(_namespace);
	};
	
	var resource = exports.resource = function resource(name, idAttr) {
	  return new _Resource2.default(name, idAttr);
	};
	
	exports.default = {
	  Resource: _Resource2.default,
	  Instance: _Instance2.default,
	  Namespace: _Namespace2.default,
	  events: { CREATE: _Evented.CREATE, CHANGE: _Evented.CHANGE, UPDATE: _Evented.UPDATE, REMOVE: _Evented.REMOVE, RESET: _Evented.RESET }
	};

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Evented2 = __webpack_require__(2);
	
	var _Evented3 = _interopRequireDefault(_Evented2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * @public
	 * @class Instance
	 * @extends {Evented}
	 */
	
	var Instance = function (_Evented) {
	  _inherits(Instance, _Evented);
	
	  /**
	   *
	   * @param {Resource} resource
	   * @param {object} props
	   * @param {Position} position
	     */
	
	  function Instance(resource) {
	    var props = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
	    var position = arguments[2];
	
	    _classCallCheck(this, Instance);
	
	    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Instance).apply(this, arguments));
	
	    _this.resource = resource;
	    _this.position = position;
	    _this.props = props;
	    _this.changes = {};
	
	    _this.registerEvents(_Evented2.CHANGE, _Evented2.UPDATE);
	    return _this;
	  }
	
	  _createClass(Instance, [{
	    key: 'getProps',
	    value: function getProps() {
	      var body = this.resource.body();
	      if (body) {
	        return this.props[body];
	      }
	      return this.props;
	    }
	
	    /**
	     * Get next Instance in the chain
	     * @returns {Instance|null}
	     */
	
	  }, {
	    key: 'next',
	    value: function next() {
	      var next = this.position.next;
	      return next && next.instance || null;
	    }
	
	    /**
	     * Get previous Instance in the chain
	     * @returns {Instance|null}
	     */
	
	  }, {
	    key: 'previous',
	    value: function previous() {
	      var previous = this.position.previous;
	      return previous && previous.instance || null;
	    }
	  }, {
	    key: 'get',
	    value: function get(key) {
	      var body = this.getProps();
	      return body[key];
	    }
	
	    /**
	     * Return the field specified by Resource#body()
	     * @returns {string|null}
	     */
	
	  }, {
	    key: 'id',
	    value: function id() {
	      var idAttribute = this.resource.idAttribute;
	
	      if (idAttribute) {
	        return this.get(idAttribute);
	      }
	      return null;
	    }
	
	    /**
	     * Return the changed value to a field
	     * @param {string} key
	     * @returns {*}
	     */
	
	  }, {
	    key: 'getChange',
	    value: function getChange(key) {
	      return this.changes[key];
	    }
	
	    /**
	     * Update a field's value
	     * @param {string} path
	     *
	     * @example
	     *
	     * Instance#resolve('a.b.c');
	     *
	     * @returns {*}
	     */
	
	  }, {
	    key: 'resolve',
	    value: function resolve() {
	      var path = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];
	
	      path = path && path.split('.') || [];
	      var props = this.props;
	
	      while (path.length > 0) {
	        var field = path.shift();
	        if (!props[field]) {
	          return;
	        }
	        props = props[field];
	      }
	
	      return props;
	    }
	
	    /**
	     * Update a body field's value
	     * @param {string} field
	     * @param {*} value
	     * @returns {Instance}
	     */
	
	  }, {
	    key: 'set',
	    value: function set(field, value) {
	      this.change(_defineProperty({}, field, value));
	      return this;
	    }
	
	    /**
	     * @param {object} changes
	     */
	
	  }, {
	    key: 'change',
	    value: function change(changes) {
	      var oldChanges = this.changes;
	      this.changes = Object.assign({}, oldChanges, changes);
	      this.publish(_Evented2.CHANGE, this, oldChanges, changes);
	    }
	
	    /**
	     * Update the resource Instance
	     * @param {object} changes fields(s) - value(s)
	     */
	
	  }, {
	    key: 'update',
	    value: function update(changes) {
	      var delta = Object.assign({}, this.changes, changes);
	      var oldProps = this.getProps();
	      var props = Object.assign({}, oldProps, delta);
	
	      // if there are any actual changes
	      if (JSON.stringify(props) !== JSON.stringify(this.props)) {
	        this.changes = {};
	        this.props = props;
	
	        this.publish(_Evented2.UPDATE, this, oldProps, delta);
	      }
	    }
	  }, {
	    key: 'remove',
	    value: function remove() {
	      this.resource.remove(this);
	    }
	
	    /**
	     * @returns {object}
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      return JSON.parse(this.toString());
	    }
	
	    /**
	     * @returns {string}
	     */
	
	  }, {
	    key: 'toString',
	    value: function toString() {
	      return JSON.stringify(this.props);
	    }
	  }]);
	
	  return Instance;
	}(_Evented3.default);
	
	exports.default = Instance;
	;

/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var CREATE = exports.CREATE = 'create';
	var CHANGE = exports.CHANGE = 'change';
	var UPDATE = exports.UPDATE = 'update';
	var REMOVE = exports.REMOVE = 'remove';
	var RESET = exports.RESET = 'reset';
	
	/**
	 * @protected
	 * @class Evented
	 */
	
	var Evented = function () {
	  function Evented() {
	    _classCallCheck(this, Evented);
	
	    this.listeners = {};
	  }
	
	  /**
	   * @param {string} type
	   */
	
	
	  _createClass(Evented, [{
	    key: 'registerEvent',
	    value: function registerEvent(type) {
	      this.listeners[type] = this.listeners[type] || [];
	    }
	
	    /**
	     * @param {string[]} types
	     */
	
	  }, {
	    key: 'registerEvents',
	    value: function registerEvents() {
	      for (var _len = arguments.length, types = Array(_len), _key = 0; _key < _len; _key++) {
	        types[_key] = arguments[_key];
	      }
	
	      types.forEach(this.registerEvent, this);
	    }
	
	    /**
	     * @param {string} type
	     * @param {*[]} payload
	     * @returns {Evented}
	     */
	
	  }, {
	    key: 'publish',
	    value: function publish(type) {
	      for (var _len2 = arguments.length, payload = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
	        payload[_key2 - 1] = arguments[_key2];
	      }
	
	      if (!this.listeners[type]) {
	        throw new Error(this.constructor.name + ' \'' + type + '\' is not a registered event!');
	      }
	      this.listeners[type].forEach(function (listener) {
	        return listener.apply(undefined, payload);
	      });
	      return this;
	    }
	
	    /**
	     * @param {string} type
	     * @param {*[]} payload
	     * @returns {Evented}
	     */
	
	  }, {
	    key: 'asyncPublish',
	    value: function asyncPublish(type) {
	      var _this = this;
	
	      for (var _len3 = arguments.length, payload = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
	        payload[_key3 - 1] = arguments[_key3];
	      }
	
	      setTimeout(function () {
	        return _this.publish.apply(_this, [type].concat(payload));
	      }, 0);
	      return this;
	    }
	
	    /**
	     * @param {string} type
	     * @param {function} listener
	     * @returns {Evented}
	     */
	
	  }, {
	    key: 'subscribe',
	    value: function subscribe(type, listener) {
	      if (typeof listener !== 'function') {
	        return;
	      }
	
	      var listeners = this.listeners[type];
	      if (listeners && listeners.indexOf(listener) === -1) {
	        listeners.push(listener);
	      }
	      return this;
	    }
	
	    /**
	     * @param {string} type
	     * @param {function} listener
	     * @returns {Evented}
	     */
	
	  }, {
	    key: 'unsubscribe',
	    value: function unsubscribe(type, listener) {
	      var listeners = this.listeners[type];
	      if (listeners) {
	        if (typeof listener !== 'function') {
	          this.listeners[type] = [];
	        }
	
	        var index = listeners.indexOf(listener);
	        if (index > -1) {
	          listeners.splice(index, 1);
	        }
	      }
	      return this;
	    }
	  }, {
	    key: 'resetEvents',
	    value: function resetEvents() {
	      var listeners = this.listeners;
	
	      this.listeners = Object.keys(listeners).reduce(function (listeners, type) {
	        listeners[type] = [];
	        return listeners;
	      }, {});
	    }
	  }]);
	
	  return Evented;
	}();

	exports.default = Evented;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Index = __webpack_require__(4);
	
	var _Index2 = _interopRequireDefault(_Index);
	
	var _Query = __webpack_require__(6);
	
	var _Query2 = _interopRequireDefault(_Query);
	
	var _Schema = __webpack_require__(7);
	
	var _Schema2 = _interopRequireDefault(_Schema);
	
	var _Position = __webpack_require__(8);
	
	var _Position2 = _interopRequireDefault(_Position);
	
	var _Instance = __webpack_require__(1);
	
	var _Instance2 = _interopRequireDefault(_Instance);
	
	var _Evented2 = __webpack_require__(2);
	
	var _Evented3 = _interopRequireDefault(_Evented2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * @class Resource
	 * @extends {Evented}
	 */
	
	var Resource = function (_Evented) {
	  _inherits(Resource, _Evented);
	
	  function Resource() {
	    var name = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];
	    var idAttribute = arguments.length <= 1 || arguments[1] === undefined ? 'id' : arguments[1];
	    var namespace = arguments.length <= 2 || arguments[2] === undefined ? null : arguments[2];
	
	    _classCallCheck(this, Resource);
	
	    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Resource).apply(this, arguments));
	
	    _this.idAttribute = idAttribute;
	    _this.namespace = namespace;
	    _this.name = name;
	    _this.order = [];
	    _this.resourceBody = null;
	    _this.Instance = _Instance2.default;
	    _this._props = {};
	    _this._schema = {};
	    _this._index = new _Index2.default(_this);
	
	    if (idAttribute) {
	      _this.index(idAttribute);
	    }
	
	    _this.registerEvents(_Evented2.CREATE, _Evented2.CHANGE, _Evented2.UPDATE, _Evented2.REMOVE, _Evented2.RESET);
	    return _this;
	  }
	
	  /**
	   * @param {Resource} position
	   * @param {object} oldProps all old props
	   * @param {object} delta changed field with new values
	     * @private
	     */
	
	
	  _createClass(Resource, [{
	    key: '_reindex',
	    value: function _reindex(_ref, oldProps, delta) {
	      var position = _ref.position;
	
	      this._index.reindexPosition(position, oldProps, delta);
	    }
	
	    /**
	     * @private
	     * @param {object} props
	     * @param {number} [index]
	     * @returns {Instance|Instance[]}
	     */
	
	  }, {
	    key: '_create',
	    value: function _create(props, index) {
	      var _this2 = this;
	
	      if (typeof index !== 'number' || isNaN(index)) {
	        index = this.order.length;
	      }
	
	      var position = new _Position2.default();
	      var previous = this.order[index - 1] || null;
	      if (previous) {
	        previous.next = position;
	      }
	      var next = this.order[index] || null;
	      if (next) {
	        next.previous = position;
	      }
	      position.next = next;
	      position.previous = previous;
	
	      var schema = _Schema2.default.get(this._schema);
	      var allProps = Object.assign({}, schema, props);
	      var Instance = this.Instance;
	
	      var instance = new Instance(this, allProps, position);
	      position.instance = instance;
	
	      this._index.indexPosition(position);
	      this.order.splice(index, 0, position);
	
	      instance.subscribe(_Evented2.UPDATE, function (instance, oldProps, delta) {
	        _this2._index.reindexPosition(instance.position, oldProps, delta);
	        _this2.publish(_Evented2.UPDATE, instance, oldProps, delta);
	      });
	
	      this.publish(_Evented2.CREATE, instance, index);
	
	      return instance;
	    }
	
	    /**
	     * @private
	     * @param {object} props
	     * @param {number} [index]
	     * @returns {Instance|Instance[]}
	     */
	
	  }, {
	    key: '_store',
	    value: function _store(props, index) {
	      var id = this.idAttribute;
	      var query = this.find(id, props[id]);
	      if (!query.isEmpty()) {
	        var instance = query.first();
	        instance.update(props);
	        return instance;
	      }
	
	      return this._create(props, index);
	    }
	
	    /**
	     * keys of instances to index
	     * @param {string[]} keys
	     * @returns {Resource}
	       */
	
	  }, {
	    key: 'index',
	    value: function index() {
	      var _index = this._index;
	
	      for (var _len = arguments.length, keys = Array(_len), _key = 0; _key < _len; _key++) {
	        keys[_key] = arguments[_key];
	      }
	
	      keys.forEach(_index.indexKey, _index);
	      return this;
	    }
	
	    /**
	     * Default keys with default values
	     * @param {object} defaults
	     * @returns {Resource}
	     */
	
	  }, {
	    key: 'defaults',
	    value: function defaults(_defaults) {
	      this._schema = _defaults;
	      return this;
	    }
	
	    /**
	     * Set the Instance class
	     * @param {Function} Instance
	     */
	
	  }, {
	    key: 'instance',
	    value: function instance(Instance) {
	      this.Instance = Instance;
	    }
	
	    /**
	     * Specify the namespace to index for an Instance
	     * This can be specified once only!
	     * @param {string} key
	     * @returns {*}
	     */
	
	  }, {
	    key: 'body',
	    value: function body(key) {
	      if (key && !this.resourceBody) {
	        this.resourceBody = key;
	        return this;
	      }
	      return this.resourceBody;
	    }
	  }, {
	    key: 'get',
	    value: function get(key) {
	      return this._props[key];
	    }
	
	    /**
	     *
	     * @param {string} key
	     * @param {*} value
	     * @returns {Resource}
	     */
	
	  }, {
	    key: 'set',
	    value: function set(key, value) {
	      this.change(_defineProperty({}, key, value));
	      return this;
	    }
	
	    /**
	     * @param {object} props
	     * @returns {Resource}
	     */
	
	  }, {
	    key: 'change',
	    value: function change(props) {
	      var _props = this._props._props;
	
	      props = Object.assign({}, _props, props);
	      this._props = props;
	      this.publish(_Evented2.CHANGE, props);
	      return this;
	    }
	
	    /**
	     * @param {object|object[]} props
	     * @param {number} [index]
	     * @returns {Resource|Resource[]}
	     */
	
	  }, {
	    key: 'store',
	    value: function store(props, index) {
	      var _this3 = this;
	
	      if (Array.isArray(props)) {
	        return props.map(function (prop, i) {
	          var at = typeof index === 'number' ? index + i : undefined;
	          return _this3._store(prop, at);
	        });
	      } else {
	        return this._store(props, index);
	      }
	    }
	
	    /**
	     * Return positions for a key and a value
	     * @param {string} key
	     * @param {*} value
	     * @returns {number[]|number}
	     */
	
	  }, {
	    key: 'position',
	    value: function position(key, value) {
	      var _this4 = this;
	
	      var positions = this._index.get(key, value);
	      return positions ? positions.map(function (pos) {
	        return _this4.order.indexOf(pos);
	      }) : -1;
	    }
	
	    /**
	     * @returns {Instance|null}
	     */
	
	  }, {
	    key: 'first',
	    value: function first() {
	      return this.at(0);
	    }
	
	    /**
	     * @returns {Instance|null}
	     */
	
	  }, {
	    key: 'last',
	    value: function last() {
	      return this.at(this.order.length - 1);
	    }
	
	    /**
	     *
	     * @param {Instance} instance
	     * @param {object} props
	     * @returns {Resource}
	     */
	
	  }, {
	    key: 'before',
	    value: function before(instance, props) {
	      var id = instance.id();
	      var index = this.position(this.idAttribute, id)[0];
	      return this.store(props, index);
	    }
	
	    /**
	     *
	     * @param {Instance} instance
	     * @param {object} props
	     * @returns {Resource}
	     */
	
	  }, {
	    key: 'after',
	    value: function after(instance, props) {
	      var id = instance.id();
	      var index = this.position(this.idAttribute, id)[0];
	      return this.store(props, index + 1);
	    }
	
	    /**
	     * @param {function} exec
	     * @returns {array}
	     */
	
	  }, {
	    key: 'all',
	    value: function all(exec) {
	      var iterator = function iterator(pos) {
	        return pos.instance;
	      };
	      if (typeof exec === 'function') {
	        iterator = function iterator(pos) {
	          for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
	            args[_key2 - 1] = arguments[_key2];
	          }
	
	          return exec.apply(undefined, [pos.instance].concat(args));
	        };
	      }
	      return this.order.map(iterator);
	    }
	
	    /**
	     * @returns {number}
	     */
	
	  }, {
	    key: 'size',
	    value: function size() {
	      return this.order.length;
	    }
	
	    /**
	     * @param {string} field
	     * @param {*} value
	     * @param {query} [query]
	     * @returns {Query}
	     */
	
	  }, {
	    key: 'find',
	    value: function find(field, value, query) {
	      var positions = this._index.get(field, value);
	      query = query || new _Query2.default();
	
	      if (positions) {
	        return query.push(positions);
	      }
	
	      var position = this.order[0];
	      while (position) {
	        if (position.instance.get(field) === value) {
	          query.push([position]);
	        }
	        position = position.next;
	      }
	
	      return query;
	    }
	
	    /**
	     * @param {object} props
	     */
	
	  }, {
	    key: 'query',
	    value: function query(props) {
	      var _this5 = this;
	
	      var query = new _Query2.default();
	      Object.keys(props).forEach(function (key) {
	        return _this5.find(key, props[key], query);
	      });
	      return query;
	    }
	
	    /**
	     * @param {string} key
	     * @returns {array}
	     */
	
	  }, {
	    key: 'values',
	    value: function values(key) {
	      return this._index.getValuesFor(key);
	    }
	
	    /**
	     * @param {number} index
	     * @returns {Instance|null}
	     */
	
	  }, {
	    key: 'at',
	    value: function at(index) {
	      var position = this.order[index];
	      return position && position.instance || null;
	    }
	
	    // convenience method only - use Collection#find/at -> Instance#update or Collection#upsert
	
	  }, {
	    key: 'update',
	    value: function update(instance, updates) {
	      instance.update(updates);
	    }
	
	    /**
	     * @param {Instance} instance
	     */
	
	  }, {
	    key: 'remove',
	    value: function remove(instance) {
	      var position = instance.position;
	
	      var index = this.order.indexOf(position);
	      if (index > -1) {
	        this.order.splice(index, 1);
	      }
	
	      this._index.removePosition(position);
	
	      var previous = position.previous;
	      var next = position.next;
	
	      if (previous) {
	        previous.next = next;
	      }
	      if (next) {
	        next.previous = previous;
	      }
	
	      instance.resetEvents();
	      this.publish(_Evented2.REMOVE, instance, index);
	    }
	
	    /**
	     * @param {string} triggerRemoves
	     */
	
	  }, {
	    key: 'reset',
	    value: function reset(triggerRemoves) {
	      var _this6 = this;
	
	      if (triggerRemoves === true) {
	        this.order.forEach(function (_ref2) {
	          var instance = _ref2.instance;
	          return _this6.remove(instance);
	        });
	      } else {
	        this.order.forEach(function (pos) {
	          pos.instance.resetEvents();
	          _this6._index.removePosition(pos);
	        });
	      }
	      this.order = [];
	      this._props = {};
	      this.publish(_Evented2.RESET);
	    }
	  }]);
	
	  return Resource;
	}(_Evented3.default);
	
	exports.default = Resource;
	;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _stringify = __webpack_require__(5);
	
	var _stringify2 = _interopRequireDefault(_stringify);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * @protected
	 * @class Index
	 */
	
	var Index = function () {
	  function Index() {
	    _classCallCheck(this, Index);
	
	    this.lookup = {};
	    this.indexKeys = [];
	  }
	
	  /**
	   * @private
	   */
	
	
	  _createClass(Index, [{
	    key: '_find',
	    value: function _find(position, executor, deleteIfEmpty) {
	      var body = position.instance.getProps();
	      var lookup = this.lookup;
	
	
	      this.indexKeys.forEach(function (key) {
	        var value = (0, _stringify2.default)(body[key]);
	        var values = lookup[key] = lookup[key] || {};
	        var positions = values[value] = values[value] || [];
	
	        executor(value, positions);
	
	        if (deleteIfEmpty && positions.length === 0) {
	          delete values[value];
	        }
	      });
	    }
	  }, {
	    key: 'indexKey',
	    value: function indexKey(key) {
	      if (this.indexKeys.indexOf(key) === -1) {
	        this.indexKeys.push(key);
	      }
	    }
	  }, {
	    key: 'getValuesFor',
	    value: function getValuesFor(key) {
	      return Object.keys(this.lookup[key] || {});
	    }
	  }, {
	    key: 'get',
	    value: function get(key, value) {
	      var field = this.lookup[key];
	      value = (0, _stringify2.default)(value);
	      return field && field[value] || null;
	    }
	  }, {
	    key: 'indexPosition',
	    value: function indexPosition(position) {
	      this._find(position, function (value, positions) {
	        position.setIndex(value, positions.length);
	        positions.push(position);
	      });
	    }
	  }, {
	    key: 'reindexPosition',
	    value: function reindexPosition(position, oldValues, delta) {
	      var lookup = this.lookup;
	
	
	      this.indexKeys.forEach(function (key) {
	        var values = lookup[key];
	        var oldValue = (0, _stringify2.default)(oldValues[key]);
	
	        if (values[oldValue]) {
	          var index = position.getIndex(oldValue);
	          values[oldValue].splice(index, 1);
	
	          if (values[oldValue].length === 0) {
	            delete values[oldValue];
	          }
	
	          position.unsetIndex(oldValue);
	        }
	
	        var newValue = (0, _stringify2.default)(delta[key]);
	        var positions = values[newValue] = values[newValue] || [];
	        position.setIndex(newValue, positions.length);
	        positions.push(position);
	      });
	    }
	  }, {
	    key: 'removePosition',
	    value: function removePosition(position) {
	      this._find(position, function (value, positions) {
	        var index = position.getIndex(value);
	        if (index > -1) {
	          positions.splice(index, 1);
	        }
	      }, true);
	    }
	  }]);
	
	  return Index;
	}();
	
	exports.default = Index;
	;

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	/**
	 * Stringify any value
	 * @param {*} value
	 * @returns {string}
	 */
	
	exports.default = function (value) {
	  var type = typeof value === 'undefined' ? 'undefined' : _typeof(value);
	
	  if (value === null) {
	    return 'null';
	  }
	
	  if (type === 'undefined') {
	    return 'undefined';
	  }
	
	  if (Array.isArray(value) || type === 'object') {
	    return JSON.stringify(value);
	  }
	
	  return '' + value;
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Query = function () {
	  function Query() {
	    _classCallCheck(this, Query);
	
	    this.results = [];
	  }
	
	  /**
	   * @param {Position[]} positions
	   */
	
	
	  _createClass(Query, [{
	    key: 'push',
	    value: function push(positions) {
	      var _this = this;
	
	      positions.forEach(function (position) {
	        if (_this.results.indexOf(position) === -1) {
	          _this.results.push(position);
	        }
	      }, this);
	      return this;
	    }
	  }, {
	    key: 'isEmpty',
	    value: function isEmpty() {
	      return this.size() === 0;
	    }
	  }, {
	    key: 'size',
	    value: function size() {
	      return this.results.length;
	    }
	  }, {
	    key: 'at',
	    value: function at(index) {
	      var pos = this.results[index];
	      return pos && pos.instance;
	    }
	  }, {
	    key: 'first',
	    value: function first() {
	      return this.at(0);
	    }
	  }, {
	    key: 'last',
	    value: function last() {
	      return this.at(this.size() - 1);
	    }
	  }, {
	    key: 'all',
	    value: function all(iterator) {
	      if (typeof iterator === 'function') {
	        return this.results.forEach(function (pos) {
	          return iterator(pos.instance);
	        });
	      }
	      return this.results.map(function (pos) {
	        return pos.instance;
	      });
	    }
	  }, {
	    key: 'sort',
	    value: function sort(comparator) {
	      this.results.sort(comparator);
	      return this;
	    }
	  }]);
	
	  return Query;
	}();
	
	exports.default = Query;
	;

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Schema = function () {
	  function Schema() {
	    _classCallCheck(this, Schema);
	  }
	
	  _createClass(Schema, null, [{
	    key: 'get',
	
	
	    /**
	     * @static
	     * @param {object} props
	     * @returns {object}
	     */
	    value: function get(props) {
	      return Object.keys(props).reduce(function (schema, key) {
	        schema[key] = Schema.defineValue(props[key]);
	        return schema;
	      }, {});
	    }
	  }, {
	    key: 'defineValue',
	
	
	    /**
	     * @static
	     * @param {*} value
	     * @returns {*}
	     */
	    value: function defineValue(value) {
	      var type = typeof value === 'undefined' ? 'undefined' : _typeof(value);
	      if (type === 'function') {
	        return new value();
	      }
	
	      if (type === 'object' && value || Array.isArray(value)) {
	        return JSON.parse(JSON.stringify(value));
	      }
	      return value;
	    }
	  }]);
	
	  return Schema;
	}();

	exports.default = Schema;

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * @class Position
	 */
	
	var Position = function () {
	  function Position() {
	    _classCallCheck(this, Position);
	
	    this.next = null;
	    this.previous = null;
	    this.instance = null;
	    this.fieldIndex = {};
	  }
	
	  /**
	   * @param {string} key
	   * @returns {*}
	   */
	
	
	  _createClass(Position, [{
	    key: "getIndex",
	    value: function getIndex(key) {
	      return this.fieldIndex[key];
	    }
	
	    /**
	     * @param {string} key
	     * @param {*} value
	     */
	
	  }, {
	    key: "setIndex",
	    value: function setIndex(key, value) {
	      this.fieldIndex[key] = value;
	    }
	
	    /**
	     * @param {string} key
	     * @returns {*}
	     */
	
	  }, {
	    key: "unsetIndex",
	    value: function unsetIndex(key) {
	      delete this.fieldIndex[key];
	    }
	  }]);
	
	  return Position;
	}();
	
	exports.default = Position;
	;

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.NAMESPACES = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Resource = __webpack_require__(3);
	
	var _Resource2 = _interopRequireDefault(_Resource);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var NAMESPACES = exports.NAMESPACES = {};
	
	/**
	 * @public
	 * @class Namespace
	 */
	
	var Namespace = function () {
	  _createClass(Namespace, null, [{
	    key: 'create',
	
	
	    /**
	     * @param {string} name
	     * @returns {Namespace}
	     */
	    value: function create(name) {
	      if (NAMESPACES[name]) {
	        return NAMESPACES[name];
	      }
	      return NAMESPACES[name] = new Namespace(name);
	    }
	
	    /**
	     * @param {string} name
	     */
	
	  }]);
	
	  function Namespace(name) {
	    _classCallCheck(this, Namespace);
	
	    if (!name) {
	      throw new TypeError('[name] must be a valid string!');
	    }
	
	    this.name = name;
	    this.keys = null;
	    this.resources = {};
	    this.idAttribute = undefined;
	    this.resourceBody = null;
	  }
	
	  /**
	   * Create or get a Resource in a Namespace
	   * @param {string} name
	   * @param {string} id - id attribute
	   * @param {Function} ctor - extended Resource constructor
	   * @returns {Resource}
	   */
	
	
	  _createClass(Namespace, [{
	    key: 'resource',
	    value: function resource(name, id, ctor) {
	      var resources = this.resources;
	
	      if (resources[name]) {
	        return resources[name];
	      }
	
	      ctor = ctor || _Resource2.default;
	      id = id || this.idAttribute;
	
	      var resource = new ctor(name, id, this);
	      var keys = this.keys;
	      var resourceBody = this.resourceBody;
	
	
	      if (keys) {
	        resource.index.apply(resource, _toConsumableArray(keys));
	      }
	
	      if (resourceBody) {
	        resource.body(resourceBody);
	      }
	
	      return resources[name] = resource;
	    }
	
	    /**
	     * Sets default idAttribute for Resources within a Namespace
	     * @param {string} idAttribute
	     * @returns {Namespace|string|undefined}
	     */
	
	  }, {
	    key: 'id',
	    value: function id(idAttribute) {
	      if (idAttribute && !this.idAttribute) {
	        this.idAttribute = idAttribute;
	        return this;
	      } else {
	        return this.idAttribute;
	      }
	    }
	
	    /**
	     * Sets default index keys for Resources within a Namespace
	     * @param {string[]} keys
	     * @returns {Namespace}
	     */
	
	  }, {
	    key: 'index',
	    value: function index() {
	      for (var _len = arguments.length, keys = Array(_len), _key = 0; _key < _len; _key++) {
	        keys[_key] = arguments[_key];
	      }
	
	      this.keys = keys;
	      return this;
	    }
	
	    /**
	     * Sets default idAttribute for Resources within a Namespace
	     * @param {string[]} body
	     * @returns {Namespace|string|undefined}
	     */
	
	  }, {
	    key: 'body',
	    value: function body(_body) {
	      if (_body && !this.resourceBody) {
	        this.resourceBody = _body;
	        return this;
	      } else {
	        return this.resourceBody;
	      }
	    }
	  }]);
	
	  return Namespace;
	}();

	exports.default = Namespace;

/***/ }
/******/ ])
});
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCA1NDA5MjgzNWVmYWNkNWZjZDZmZCIsIndlYnBhY2s6Ly8vLi9saWIvbWFpbi5qcyIsIndlYnBhY2s6Ly8vLi9saWIvSW5zdGFuY2UuanMiLCJ3ZWJwYWNrOi8vLy4vbGliL0V2ZW50ZWQuanMiLCJ3ZWJwYWNrOi8vLy4vbGliL1Jlc291cmNlLmpzIiwid2VicGFjazovLy8uL2xpYi9JbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9saWIvc3RyaW5naWZ5LmpzIiwid2VicGFjazovLy8uL2xpYi9RdWVyeS5qcyIsIndlYnBhY2s6Ly8vLi9saWIvU2NoZW1hLmpzIiwid2VicGFjazovLy8uL2xpYi9Qb3NpdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9saWIvTmFtZXNwYWNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO0FDVkE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsdUJBQWU7QUFDZjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFTyxLQUFNLGdDQUFZLG1CQUFDLFVBQUQ7VUFBZSxvQkFBVSxNQUFWLENBQWlCLFVBQWpCO0VBQWY7O0FBRWxCLEtBQU0sOEJBQVcsU0FBWCxRQUFXLENBQUMsSUFBRCxFQUFPLE1BQVA7VUFBa0IsdUJBQWEsSUFBYixFQUFtQixNQUFuQjtFQUFsQjs7bUJBRVQ7QUFDYiwrQkFEYTtBQUViLCtCQUZhO0FBR2IsaUNBSGE7QUFJYixXQUFRLEVBQUUsdUJBQUYsRUFBVSx1QkFBVixFQUFrQix1QkFBbEIsRUFBMEIsdUJBQTFCLEVBQWtDLHFCQUFsQyxFQUFSOzs7Ozs7Ozs7Ozs7Ozs7QUNkRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0FPcUI7Ozs7Ozs7Ozs7QUFRbkIsWUFSbUIsUUFRbkIsQ0FBWSxRQUFaLEVBQTRDO1NBQXRCLDhEQUFRLGtCQUFjO1NBQVYsd0JBQVU7OzJCQVJ6QixVQVF5Qjs7d0VBUnpCLHNCQVNSLFlBRGlDOztBQUcxQyxXQUFLLFFBQUwsR0FBZ0IsUUFBaEIsQ0FIMEM7QUFJMUMsV0FBSyxRQUFMLEdBQWdCLFFBQWhCLENBSjBDO0FBSzFDLFdBQUssS0FBTCxHQUFhLEtBQWIsQ0FMMEM7QUFNMUMsV0FBSyxPQUFMLEdBQWUsRUFBZixDQU4wQzs7QUFRMUMsV0FBSyxjQUFMLHFDQVIwQzs7SUFBNUM7O2dCQVJtQjs7Z0NBbUJSO0FBQ1QsV0FBTSxPQUFRLEtBQUssUUFBTCxDQUFjLElBQWQsRUFBUixDQURHO0FBRVQsV0FBSSxJQUFKLEVBQVU7QUFDUixnQkFBTyxLQUFLLEtBQUwsQ0FBVyxJQUFYLENBQVAsQ0FEUTtRQUFWO0FBR0EsY0FBTyxLQUFLLEtBQUwsQ0FMRTs7Ozs7Ozs7Ozs0QkFZSjtBQUNMLFdBQU0sT0FBTyxLQUFLLFFBQUwsQ0FBYyxJQUFkLENBRFI7QUFFTCxjQUFRLFFBQVEsS0FBSyxRQUFMLElBQWlCLElBQXpCLENBRkg7Ozs7Ozs7Ozs7Z0NBU0k7QUFDVCxXQUFNLFdBQVcsS0FBSyxRQUFMLENBQWMsUUFBZCxDQURSO0FBRVQsY0FBUSxZQUFZLFNBQVMsUUFBVCxJQUFxQixJQUFqQyxDQUZDOzs7O3lCQUtQLEtBQUs7QUFDUCxXQUFNLE9BQU8sS0FBSyxRQUFMLEVBQVAsQ0FEQztBQUVQLGNBQU8sS0FBSyxHQUFMLENBQVAsQ0FGTzs7Ozs7Ozs7OzswQkFTSjtXQUNLLGNBQWdCLEtBQUssUUFBTCxDQUFoQixZQURMOztBQUVILFdBQUksV0FBSixFQUFpQjtBQUNmLGdCQUFPLEtBQUssR0FBTCxDQUFTLFdBQVQsQ0FBUCxDQURlO1FBQWpCO0FBR0EsY0FBTyxJQUFQLENBTEc7Ozs7Ozs7Ozs7OytCQWFLLEtBQUs7QUFDYixjQUFPLEtBQUssT0FBTCxDQUFhLEdBQWIsQ0FBUCxDQURhOzs7Ozs7Ozs7Ozs7Ozs7OytCQWNJO1dBQVgsNkRBQU8sa0JBQUk7O0FBQ2pCLGNBQVEsUUFBUSxLQUFLLEtBQUwsQ0FBVyxHQUFYLENBQVIsSUFBMkIsRUFBM0IsQ0FEUztBQUVqQixXQUFJLFFBQVEsS0FBSyxLQUFMLENBRks7O0FBSWpCLGNBQU8sS0FBSyxNQUFMLEdBQWMsQ0FBZCxFQUFpQjtBQUN0QixhQUFNLFFBQVEsS0FBSyxLQUFMLEVBQVIsQ0FEZ0I7QUFFdEIsYUFBSSxDQUFDLE1BQU0sS0FBTixDQUFELEVBQWU7QUFDakIsa0JBRGlCO1VBQW5CO0FBR0EsaUJBQVEsTUFBTSxLQUFOLENBQVIsQ0FMc0I7UUFBeEI7O0FBUUEsY0FBTyxLQUFQLENBWmlCOzs7Ozs7Ozs7Ozs7eUJBcUJmLE9BQU8sT0FBTztBQUNoQixZQUFLLE1BQUwscUJBQWUsT0FBUSxNQUF2QixFQURnQjtBQUVoQixjQUFPLElBQVAsQ0FGZ0I7Ozs7Ozs7Ozs0QkFRWCxTQUFTO0FBQ2QsV0FBTSxhQUFhLEtBQUssT0FBTCxDQURMO0FBRWQsWUFBSyxPQUFMLEdBQWUsT0FBTyxNQUFQLENBQWMsRUFBZCxFQUFrQixVQUFsQixFQUE4QixPQUE5QixDQUFmLENBRmM7QUFHZCxZQUFLLE9BQUwsbUJBQXFCLElBQXJCLEVBQTJCLFVBQTNCLEVBQXVDLE9BQXZDLEVBSGM7Ozs7Ozs7Ozs7NEJBVVQsU0FBUztBQUNkLFdBQU0sUUFBUSxPQUFPLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUssT0FBTCxFQUFjLE9BQWhDLENBQVIsQ0FEUTtBQUVkLFdBQU0sV0FBVyxLQUFLLFFBQUwsRUFBWCxDQUZRO0FBR2QsV0FBTSxRQUFRLE9BQU8sTUFBUCxDQUFjLEVBQWQsRUFBa0IsUUFBbEIsRUFBNEIsS0FBNUIsQ0FBUjs7O0FBSFEsV0FNVixLQUFLLFNBQUwsQ0FBZSxLQUFmLE1BQTBCLEtBQUssU0FBTCxDQUFlLEtBQUssS0FBTCxDQUF6QyxFQUFzRDtBQUN4RCxjQUFLLE9BQUwsR0FBZSxFQUFmLENBRHdEO0FBRXhELGNBQUssS0FBTCxHQUFhLEtBQWIsQ0FGd0Q7O0FBSXhELGNBQUssT0FBTCxtQkFBcUIsSUFBckIsRUFBMkIsUUFBM0IsRUFBcUMsS0FBckMsRUFKd0Q7UUFBMUQ7Ozs7OEJBUU87QUFDUCxZQUFLLFFBQUwsQ0FBYyxNQUFkLENBQXFCLElBQXJCLEVBRE87Ozs7Ozs7Ozs4QkFPQTtBQUNQLGNBQU8sS0FBSyxLQUFMLENBQVcsS0FBSyxRQUFMLEVBQVgsQ0FBUCxDQURPOzs7Ozs7Ozs7Z0NBT0U7QUFDVCxjQUFPLEtBQUssU0FBTCxDQUFlLEtBQUssS0FBTCxDQUF0QixDQURTOzs7O1VBcEpROzs7O0FBdUpwQixFOzs7Ozs7Ozs7Ozs7Ozs7O0FDOUpNLEtBQU0sMEJBQVMsUUFBVDtBQUNOLEtBQU0sMEJBQVMsUUFBVDtBQUNOLEtBQU0sMEJBQVMsUUFBVDtBQUNOLEtBQU0sMEJBQVMsUUFBVDtBQUNOLEtBQU0sd0JBQVEsT0FBUjs7Ozs7OztLQU1RO0FBRW5CLFlBRm1CLE9BRW5CLEdBQWM7MkJBRkssU0FFTDs7QUFDWixVQUFLLFNBQUwsR0FBaUIsRUFBakIsQ0FEWTtJQUFkOzs7Ozs7O2dCQUZtQjs7bUNBU0wsTUFBTTtBQUNsQixZQUFLLFNBQUwsQ0FBZSxJQUFmLElBQXdCLEtBQUssU0FBTCxDQUFlLElBQWYsS0FBd0IsRUFBeEIsQ0FETjs7Ozs7Ozs7O3NDQU9LO3lDQUFQOztRQUFPOztBQUN2QixhQUFNLE9BQU4sQ0FBYyxLQUFLLGFBQUwsRUFBb0IsSUFBbEMsRUFEdUI7Ozs7Ozs7Ozs7OzZCQVNqQixNQUFrQjswQ0FBVDs7UUFBUzs7QUFDeEIsV0FBSSxDQUFDLEtBQUssU0FBTCxDQUFlLElBQWYsQ0FBRCxFQUF1QjtBQUN6QixlQUFNLElBQUksS0FBSixDQUFhLEtBQUssV0FBTCxDQUFpQixJQUFqQixXQUEwQixzQ0FBdkMsQ0FBTixDQUR5QjtRQUEzQjtBQUdBLFlBQUssU0FBTCxDQUFlLElBQWYsRUFBcUIsT0FBckIsQ0FBNkI7Z0JBQVksMEJBQVksT0FBWjtRQUFaLENBQTdCLENBSndCO0FBS3hCLGNBQU8sSUFBUCxDQUx3Qjs7Ozs7Ozs7Ozs7a0NBYWIsTUFBa0I7OzswQ0FBVDs7UUFBUzs7QUFDN0Isa0JBQVc7Z0JBQU0sTUFBSyxPQUFMLGVBQWEsYUFBUyxRQUF0QjtRQUFOLEVBQXNDLENBQWpELEVBRDZCO0FBRTdCLGNBQU8sSUFBUCxDQUY2Qjs7Ozs7Ozs7Ozs7K0JBVXJCLE1BQU0sVUFBVTtBQUN4QixXQUFJLE9BQU8sUUFBUCxLQUFvQixVQUFwQixFQUFnQztBQUNsQyxnQkFEa0M7UUFBcEM7O0FBSUEsV0FBTSxZQUFZLEtBQUssU0FBTCxDQUFlLElBQWYsQ0FBWixDQUxrQjtBQU14QixXQUFJLGFBQWEsVUFBVSxPQUFWLENBQWtCLFFBQWxCLE1BQWdDLENBQUMsQ0FBRCxFQUFJO0FBQ25ELG1CQUFVLElBQVYsQ0FBZSxRQUFmLEVBRG1EO1FBQXJEO0FBR0EsY0FBTyxJQUFQLENBVHdCOzs7Ozs7Ozs7OztpQ0FpQmQsTUFBTSxVQUFVO0FBQzFCLFdBQU0sWUFBWSxLQUFLLFNBQUwsQ0FBZSxJQUFmLENBQVosQ0FEb0I7QUFFMUIsV0FBSSxTQUFKLEVBQWU7QUFDYixhQUFJLE9BQU8sUUFBUCxLQUFvQixVQUFwQixFQUFnQztBQUNsQyxnQkFBSyxTQUFMLENBQWUsSUFBZixJQUF1QixFQUF2QixDQURrQztVQUFwQzs7QUFJQSxhQUFNLFFBQVEsVUFBVSxPQUFWLENBQWtCLFFBQWxCLENBQVIsQ0FMTztBQU1iLGFBQUksUUFBUSxDQUFDLENBQUQsRUFBSTtBQUNkLHFCQUFVLE1BQVYsQ0FBaUIsS0FBakIsRUFBd0IsQ0FBeEIsRUFEYztVQUFoQjtRQU5GO0FBVUEsY0FBTyxJQUFQLENBWjBCOzs7O21DQWVkO1dBQ0osWUFBYyxLQUFkLFVBREk7O0FBRVosWUFBSyxTQUFMLEdBQWlCLE9BQU8sSUFBUCxDQUFZLFNBQVosRUFBdUIsTUFBdkIsQ0FBOEIsVUFBQyxTQUFELEVBQVksSUFBWixFQUFxQjtBQUNsRSxtQkFBVSxJQUFWLElBQWtCLEVBQWxCLENBRGtFO0FBRWxFLGdCQUFPLFNBQVAsQ0FGa0U7UUFBckIsRUFHNUMsRUFIYyxDQUFqQixDQUZZOzs7O1VBaEZLOzs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZyQjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0FNcUI7OztBQUVuQixZQUZtQixRQUVuQixHQUE2RDtTQUFqRCw2REFBTyxrQkFBMEM7U0FBdEMsb0VBQWMsb0JBQXdCO1NBQWxCLGtFQUFZLG9CQUFNOzsyQkFGMUMsVUFFMEM7O3dFQUYxQyxzQkFHUixZQURrRDs7QUFHM0QsV0FBSyxXQUFMLEdBQW1CLFdBQW5CLENBSDJEO0FBSTNELFdBQUssU0FBTCxHQUFpQixTQUFqQixDQUoyRDtBQUszRCxXQUFLLElBQUwsR0FBWSxJQUFaLENBTDJEO0FBTTNELFdBQUssS0FBTCxHQUFhLEVBQWIsQ0FOMkQ7QUFPM0QsV0FBSyxZQUFMLEdBQW9CLElBQXBCLENBUDJEO0FBUTNELFdBQUssUUFBTCxzQkFSMkQ7QUFTM0QsV0FBSyxNQUFMLEdBQWMsRUFBZCxDQVQyRDtBQVUzRCxXQUFLLE9BQUwsR0FBZSxFQUFmLENBVjJEO0FBVzNELFdBQUssTUFBTCxHQUFjLDBCQUFkLENBWDJEOztBQWEzRCxTQUFJLFdBQUosRUFBaUI7QUFDZixhQUFLLEtBQUwsQ0FBVyxXQUFYLEVBRGU7TUFBakI7O0FBSUEsV0FBSyxjQUFMLDBGQWpCMkQ7O0lBQTdEOzs7Ozs7Ozs7O2dCQUZtQjs7b0NBNEJJLFVBQVUsT0FBTztXQUE3Qix5QkFBNkI7O0FBQ3RDLFlBQUssTUFBTCxDQUFZLGVBQVosQ0FBNEIsUUFBNUIsRUFBc0MsUUFBdEMsRUFBZ0QsS0FBaEQsRUFEc0M7Ozs7Ozs7Ozs7Ozs2QkFVaEMsT0FBTyxPQUFPOzs7QUFDcEIsV0FBSSxPQUFPLEtBQVAsS0FBaUIsUUFBakIsSUFBNkIsTUFBTSxLQUFOLENBQTdCLEVBQTJDO0FBQzdDLGlCQUFRLEtBQUssS0FBTCxDQUFXLE1BQVgsQ0FEcUM7UUFBL0M7O0FBSUEsV0FBTSxXQUFXLHdCQUFYLENBTGM7QUFNcEIsV0FBTSxXQUFZLEtBQUssS0FBTCxDQUFXLFFBQVEsQ0FBUixDQUFYLElBQXlCLElBQXpCLENBTkU7QUFPcEIsV0FBSSxRQUFKLEVBQWM7QUFDWixrQkFBUyxJQUFULEdBQWdCLFFBQWhCLENBRFk7UUFBZDtBQUdBLFdBQU0sT0FBUSxLQUFLLEtBQUwsQ0FBVyxLQUFYLEtBQXFCLElBQXJCLENBVk07QUFXcEIsV0FBSSxJQUFKLEVBQVU7QUFDUixjQUFLLFFBQUwsR0FBZ0IsUUFBaEIsQ0FEUTtRQUFWO0FBR0EsZ0JBQVMsSUFBVCxHQUFnQixJQUFoQixDQWRvQjtBQWVwQixnQkFBUyxRQUFULEdBQW9CLFFBQXBCLENBZm9COztBQWlCcEIsV0FBTSxTQUFTLGlCQUFPLEdBQVAsQ0FBVyxLQUFLLE9BQUwsQ0FBcEIsQ0FqQmM7QUFrQnBCLFdBQU0sV0FBVyxPQUFPLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLE1BQWxCLEVBQTBCLEtBQTFCLENBQVgsQ0FsQmM7V0FtQlosV0FBYSxLQUFiLFNBbkJZOztBQW9CcEIsV0FBTSxXQUFXLElBQUksUUFBSixDQUFhLElBQWIsRUFBbUIsUUFBbkIsRUFBNkIsUUFBN0IsQ0FBWCxDQXBCYztBQXFCcEIsZ0JBQVMsUUFBVCxHQUFvQixRQUFwQixDQXJCb0I7O0FBdUJwQixZQUFLLE1BQUwsQ0FBWSxhQUFaLENBQTBCLFFBQTFCLEVBdkJvQjtBQXdCcEIsWUFBSyxLQUFMLENBQVcsTUFBWCxDQUFrQixLQUFsQixFQUF5QixDQUF6QixFQUE0QixRQUE1QixFQXhCb0I7O0FBMEJwQixnQkFBUyxTQUFULG1CQUEyQixVQUFDLFFBQUQsRUFBVyxRQUFYLEVBQXFCLEtBQXJCLEVBQStCO0FBQ3hELGdCQUFLLE1BQUwsQ0FBWSxlQUFaLENBQTRCLFNBQVMsUUFBVCxFQUFtQixRQUEvQyxFQUF5RCxLQUF6RCxFQUR3RDtBQUV4RCxnQkFBSyxPQUFMLG1CQUFxQixRQUFyQixFQUErQixRQUEvQixFQUF5QyxLQUF6QyxFQUZ3RDtRQUEvQixDQUEzQixDQTFCb0I7O0FBK0JwQixZQUFLLE9BQUwsbUJBQXFCLFFBQXJCLEVBQStCLEtBQS9CLEVBL0JvQjs7QUFpQ3BCLGNBQU8sUUFBUCxDQWpDb0I7Ozs7Ozs7Ozs7Ozs0QkEwQ2YsT0FBTyxPQUFPO0FBQ25CLFdBQU0sS0FBSyxLQUFLLFdBQUwsQ0FEUTtBQUVuQixXQUFJLFFBQVEsS0FBSyxJQUFMLENBQVUsRUFBVixFQUFjLE1BQU0sRUFBTixDQUFkLENBQVIsQ0FGZTtBQUduQixXQUFJLENBQUMsTUFBTSxPQUFOLEVBQUQsRUFBa0I7QUFDcEIsYUFBTSxXQUFXLE1BQU0sS0FBTixFQUFYLENBRGM7QUFFcEIsa0JBQVMsTUFBVCxDQUFnQixLQUFoQixFQUZvQjtBQUdwQixnQkFBTyxRQUFQLENBSG9CO1FBQXRCOztBQU1BLGNBQU8sS0FBSyxPQUFMLENBQWEsS0FBYixFQUFvQixLQUFwQixDQUFQLENBVG1COzs7Ozs7Ozs7Ozs2QkFpQk47V0FDTCxTQUFXLEtBQVgsT0FESzs7eUNBQU47O1FBQU07O0FBRWIsWUFBSyxPQUFMLENBQWEsT0FBTyxRQUFQLEVBQWlCLE1BQTlCLEVBRmE7QUFHYixjQUFPLElBQVAsQ0FIYTs7Ozs7Ozs7Ozs7OEJBV04sV0FBVTtBQUNqQixZQUFLLE9BQUwsR0FBZSxTQUFmLENBRGlCO0FBRWpCLGNBQU8sSUFBUCxDQUZpQjs7Ozs7Ozs7Ozs4QkFTVixVQUFVO0FBQ2pCLFlBQUssUUFBTCxHQUFnQixRQUFoQixDQURpQjs7Ozs7Ozs7Ozs7OzBCQVVkLEtBQUs7QUFDUixXQUFJLE9BQU8sQ0FBQyxLQUFLLFlBQUwsRUFBbUI7QUFDN0IsY0FBSyxZQUFMLEdBQW9CLEdBQXBCLENBRDZCO0FBRTdCLGdCQUFPLElBQVAsQ0FGNkI7UUFBL0I7QUFJQSxjQUFPLEtBQUssWUFBTCxDQUxDOzs7O3lCQVFOLEtBQUs7QUFDUCxjQUFPLEtBQUssTUFBTCxDQUFZLEdBQVosQ0FBUCxDQURPOzs7Ozs7Ozs7Ozs7eUJBVUwsS0FBSyxPQUFPO0FBQ2QsWUFBSyxNQUFMLHFCQUFlLEtBQU0sTUFBckIsRUFEYztBQUVkLGNBQU8sSUFBUCxDQUZjOzs7Ozs7Ozs7OzRCQVNULE9BQU87V0FDSixTQUFXLEtBQUssTUFBTCxDQUFYLE9BREk7O0FBRVosZUFBUSxPQUFPLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLE1BQWxCLEVBQTBCLEtBQTFCLENBQVIsQ0FGWTtBQUdaLFlBQUssTUFBTCxHQUFjLEtBQWQsQ0FIWTtBQUlaLFlBQUssT0FBTCxtQkFBcUIsS0FBckIsRUFKWTtBQUtaLGNBQU8sSUFBUCxDQUxZOzs7Ozs7Ozs7OzsyQkFhUixPQUFPLE9BQU87OztBQUNsQixXQUFJLE1BQU0sT0FBTixDQUFjLEtBQWQsQ0FBSixFQUEwQjtBQUN4QixnQkFBTyxNQUFNLEdBQU4sQ0FBVSxVQUFDLElBQUQsRUFBTyxDQUFQLEVBQWE7QUFDNUIsZUFBTSxLQUFPLE9BQU8sS0FBUCxLQUFpQixRQUFqQixHQUE0QixRQUFRLENBQVIsR0FBWSxTQUF4QyxDQURlO0FBRTVCLGtCQUFPLE9BQUssTUFBTCxDQUFZLElBQVosRUFBa0IsRUFBbEIsQ0FBUCxDQUY0QjtVQUFiLENBQWpCLENBRHdCO1FBQTFCLE1BS087QUFDTCxnQkFBTyxLQUFLLE1BQUwsQ0FBWSxLQUFaLEVBQW1CLEtBQW5CLENBQVAsQ0FESztRQUxQOzs7Ozs7Ozs7Ozs7OEJBZ0JPLEtBQUssT0FBTzs7O0FBQ25CLFdBQU0sWUFBWSxLQUFLLE1BQUwsQ0FBWSxHQUFaLENBQWdCLEdBQWhCLEVBQXFCLEtBQXJCLENBQVosQ0FEYTtBQUVuQixjQUFRLFlBQVksVUFBVSxHQUFWLENBQWM7Z0JBQU8sT0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQixHQUFuQjtRQUFQLENBQTFCLEdBQTRELENBQUMsQ0FBRCxDQUZqRDs7Ozs7Ozs7OzZCQVFiO0FBQ04sY0FBTyxLQUFLLEVBQUwsQ0FBUSxDQUFSLENBQVAsQ0FETTs7Ozs7Ozs7OzRCQU9EO0FBQ0wsY0FBTyxLQUFLLEVBQUwsQ0FBUSxLQUFLLEtBQUwsQ0FBVyxNQUFYLEdBQW9CLENBQXBCLENBQWYsQ0FESzs7Ozs7Ozs7Ozs7OzRCQVVBLFVBQVUsT0FBTztBQUN0QixXQUFNLEtBQUssU0FBUyxFQUFULEVBQUwsQ0FEZ0I7QUFFdEIsV0FBTSxRQUFRLEtBQUssUUFBTCxDQUFjLEtBQUssV0FBTCxFQUFrQixFQUFoQyxFQUFvQyxDQUFwQyxDQUFSLENBRmdCO0FBR3RCLGNBQU8sS0FBSyxLQUFMLENBQVcsS0FBWCxFQUFrQixLQUFsQixDQUFQLENBSHNCOzs7Ozs7Ozs7Ozs7MkJBWWxCLFVBQVUsT0FBTztBQUNyQixXQUFNLEtBQUssU0FBUyxFQUFULEVBQUwsQ0FEZTtBQUVyQixXQUFNLFFBQVEsS0FBSyxRQUFMLENBQWMsS0FBSyxXQUFMLEVBQWtCLEVBQWhDLEVBQW9DLENBQXBDLENBQVIsQ0FGZTtBQUdyQixjQUFPLEtBQUssS0FBTCxDQUFXLEtBQVgsRUFBa0IsUUFBUSxDQUFSLENBQXpCLENBSHFCOzs7Ozs7Ozs7O3lCQVVuQixNQUFNO0FBQ1IsV0FBSSxXQUFXO2dCQUFPLElBQUksUUFBSjtRQUFQLENBRFA7QUFFUixXQUFJLE9BQU8sSUFBUCxLQUFnQixVQUFoQixFQUE0QjtBQUM5QixvQkFBVyxrQkFBQyxHQUFEOzhDQUFTOzs7O2tCQUFTLHVCQUFLLElBQUksUUFBSixTQUFpQixLQUF0QjtVQUFsQixDQURtQjtRQUFoQztBQUdBLGNBQU8sS0FBSyxLQUFMLENBQVcsR0FBWCxDQUFlLFFBQWYsQ0FBUCxDQUxROzs7Ozs7Ozs7NEJBV0g7QUFDTCxjQUFPLEtBQUssS0FBTCxDQUFXLE1BQVgsQ0FERjs7Ozs7Ozs7Ozs7OzBCQVVGLE9BQU8sT0FBTyxPQUFPO0FBQ3hCLFdBQU0sWUFBWSxLQUFLLE1BQUwsQ0FBWSxHQUFaLENBQWdCLEtBQWhCLEVBQXVCLEtBQXZCLENBQVosQ0FEa0I7QUFFeEIsZUFBUyxTQUFTLHFCQUFULENBRmU7O0FBSXhCLFdBQUksU0FBSixFQUFlO0FBQ2IsZ0JBQU8sTUFBTSxJQUFOLENBQVcsU0FBWCxDQUFQLENBRGE7UUFBZjs7QUFJQSxXQUFJLFdBQVcsS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFYLENBUm9CO0FBU3hCLGNBQU8sUUFBUCxFQUFpQjtBQUNmLGFBQUksU0FBUyxRQUFULENBQWtCLEdBQWxCLENBQXNCLEtBQXRCLE1BQWlDLEtBQWpDLEVBQXdDO0FBQzFDLGlCQUFNLElBQU4sQ0FBVyxDQUFDLFFBQUQsQ0FBWCxFQUQwQztVQUE1QztBQUdBLG9CQUFXLFNBQVMsSUFBVCxDQUpJO1FBQWpCOztBQU9BLGNBQU8sS0FBUCxDQWhCd0I7Ozs7Ozs7OzsyQkFzQnBCLE9BQU87OztBQUNYLFdBQU0sUUFBUSxxQkFBUixDQURLO0FBRVgsY0FBTyxJQUFQLENBQVksS0FBWixFQUFtQixPQUFuQixDQUEyQjtnQkFBTyxPQUFLLElBQUwsQ0FBVSxHQUFWLEVBQWUsTUFBTSxHQUFOLENBQWYsRUFBMkIsS0FBM0I7UUFBUCxDQUEzQixDQUZXO0FBR1gsY0FBTyxLQUFQLENBSFc7Ozs7Ozs7Ozs7NEJBVU4sS0FBSztBQUNWLGNBQU8sS0FBSyxNQUFMLENBQVksWUFBWixDQUF5QixHQUF6QixDQUFQLENBRFU7Ozs7Ozs7Ozs7d0JBUVQsT0FBTztBQUNSLFdBQU0sV0FBVyxLQUFLLEtBQUwsQ0FBVyxLQUFYLENBQVgsQ0FERTtBQUVSLGNBQVEsWUFBWSxTQUFTLFFBQVQsSUFBcUIsSUFBakMsQ0FGQTs7Ozs7Ozs0QkFNSCxVQUFVLFNBQVM7QUFDeEIsZ0JBQVMsTUFBVCxDQUFnQixPQUFoQixFQUR3Qjs7Ozs7Ozs7OzRCQU9uQixVQUFVO1dBQ1AsV0FBYSxTQUFiLFNBRE87O0FBRWYsV0FBTSxRQUFRLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FBbUIsUUFBbkIsQ0FBUixDQUZTO0FBR2YsV0FBSSxRQUFRLENBQUMsQ0FBRCxFQUFJO0FBQ2QsY0FBSyxLQUFMLENBQVcsTUFBWCxDQUFrQixLQUFsQixFQUF5QixDQUF6QixFQURjO1FBQWhCOztBQUlBLFlBQUssTUFBTCxDQUFZLGNBQVosQ0FBMkIsUUFBM0IsRUFQZTs7V0FTUCxXQUFtQixTQUFuQixTQVRPO1dBU0csT0FBUyxTQUFULEtBVEg7O0FBVWYsV0FBSSxRQUFKLEVBQWM7QUFDWixrQkFBUyxJQUFULEdBQWdCLElBQWhCLENBRFk7UUFBZDtBQUdBLFdBQUksSUFBSixFQUFVO0FBQ1IsY0FBSyxRQUFMLEdBQWdCLFFBQWhCLENBRFE7UUFBVjs7QUFJQSxnQkFBUyxXQUFULEdBakJlO0FBa0JmLFlBQUssT0FBTCxtQkFBcUIsUUFBckIsRUFBK0IsS0FBL0IsRUFsQmU7Ozs7Ozs7OzsyQkF3QlgsZ0JBQWdCOzs7QUFDcEIsV0FBSSxtQkFBbUIsSUFBbkIsRUFBeUI7QUFDM0IsY0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQjtlQUFHO2tCQUFlLE9BQUssTUFBTCxDQUFZLFFBQVo7VUFBbEIsQ0FBbkIsQ0FEMkI7UUFBN0IsTUFFTztBQUNMLGNBQUssS0FBTCxDQUFXLE9BQVgsQ0FBbUIsZUFBTztBQUN4QixlQUFJLFFBQUosQ0FBYSxXQUFiLEdBRHdCO0FBRXhCLGtCQUFLLE1BQUwsQ0FBWSxjQUFaLENBQTJCLEdBQTNCLEVBRndCO1VBQVAsQ0FBbkIsQ0FESztRQUZQO0FBUUEsWUFBSyxLQUFMLEdBQWEsRUFBYixDQVRvQjtBQVVwQixZQUFLLE1BQUwsR0FBYyxFQUFkLENBVm9CO0FBV3BCLFlBQUssT0FBTCxrQkFYb0I7Ozs7VUF6VUg7Ozs7QUF1VnBCLEU7Ozs7Ozs7Ozs7Ozs7O0FDbFdEOzs7Ozs7Ozs7Ozs7O0tBTXFCO0FBRW5CLFlBRm1CLEtBRW5CLEdBQWM7MkJBRkssT0FFTDs7QUFDWixVQUFLLE1BQUwsR0FBYyxFQUFkLENBRFk7QUFFWixVQUFLLFNBQUwsR0FBaUIsRUFBakIsQ0FGWTtJQUFkOzs7Ozs7O2dCQUZtQjs7MkJBVWIsVUFBVSxVQUFVLGVBQWU7QUFDdkMsV0FBTSxPQUFPLFNBQVMsUUFBVCxDQUFrQixRQUFsQixFQUFQLENBRGlDO1dBRS9CLFNBQVcsS0FBWCxPQUYrQjs7O0FBSXZDLFlBQUssU0FBTCxDQUFlLE9BQWYsQ0FBdUIsZUFBTztBQUM1QixhQUFNLFFBQVEseUJBQVUsS0FBSyxHQUFMLENBQVYsQ0FBUixDQURzQjtBQUU1QixhQUFNLFNBQVMsT0FBTyxHQUFQLElBQWUsT0FBTyxHQUFQLEtBQWUsRUFBZixDQUZGO0FBRzVCLGFBQU0sWUFBWSxPQUFPLEtBQVAsSUFBaUIsT0FBTyxLQUFQLEtBQWlCLEVBQWpCLENBSFA7O0FBSzVCLGtCQUFTLEtBQVQsRUFBZ0IsU0FBaEIsRUFMNEI7O0FBTzVCLGFBQUksaUJBQWlCLFVBQVUsTUFBVixLQUFxQixDQUFyQixFQUF3QjtBQUMzQyxrQkFBTyxPQUFPLEtBQVAsQ0FBUCxDQUQyQztVQUE3QztRQVBxQixDQUF2QixDQUp1Qzs7Ozs4QkFpQmhDLEtBQUs7QUFDWixXQUFJLEtBQUssU0FBTCxDQUFlLE9BQWYsQ0FBdUIsR0FBdkIsTUFBZ0MsQ0FBQyxDQUFELEVBQUk7QUFDdEMsY0FBSyxTQUFMLENBQWUsSUFBZixDQUFvQixHQUFwQixFQURzQztRQUF4Qzs7OztrQ0FLVyxLQUFLO0FBQ2hCLGNBQU8sT0FBTyxJQUFQLENBQVksS0FBSyxNQUFMLENBQVksR0FBWixLQUFvQixFQUFwQixDQUFuQixDQURnQjs7Ozt5QkFJZCxLQUFLLE9BQU87QUFDZCxXQUFNLFFBQVEsS0FBSyxNQUFMLENBQVksR0FBWixDQUFSLENBRFE7QUFFZCxlQUFRLHlCQUFVLEtBQVYsQ0FBUixDQUZjO0FBR2QsY0FBUSxTQUFTLE1BQU0sS0FBTixDQUFULElBQXlCLElBQXpCLENBSE07Ozs7bUNBTUYsVUFBVTtBQUN0QixZQUFLLEtBQUwsQ0FBVyxRQUFYLEVBQXFCLFVBQUMsS0FBRCxFQUFRLFNBQVIsRUFBc0I7QUFDekMsa0JBQVMsUUFBVCxDQUFrQixLQUFsQixFQUF5QixVQUFVLE1BQVYsQ0FBekIsQ0FEeUM7QUFFekMsbUJBQVUsSUFBVixDQUFlLFFBQWYsRUFGeUM7UUFBdEIsQ0FBckIsQ0FEc0I7Ozs7cUNBT1IsVUFBVSxXQUFXLE9BQU87V0FDbEMsU0FBVyxLQUFYLE9BRGtDOzs7QUFHMUMsWUFBSyxTQUFMLENBQWUsT0FBZixDQUF1QixlQUFPO0FBQzVCLGFBQU0sU0FBUyxPQUFPLEdBQVAsQ0FBVCxDQURzQjtBQUU1QixhQUFNLFdBQVcseUJBQVUsVUFBVSxHQUFWLENBQVYsQ0FBWCxDQUZzQjs7QUFJNUIsYUFBSSxPQUFPLFFBQVAsQ0FBSixFQUFzQjtBQUNwQixlQUFNLFFBQVEsU0FBUyxRQUFULENBQWtCLFFBQWxCLENBQVIsQ0FEYztBQUVwQixrQkFBTyxRQUFQLEVBQWlCLE1BQWpCLENBQXdCLEtBQXhCLEVBQStCLENBQS9CLEVBRm9COztBQUlwQixlQUFJLE9BQU8sUUFBUCxFQUFpQixNQUFqQixLQUE0QixDQUE1QixFQUErQjtBQUNqQyxvQkFBTyxPQUFPLFFBQVAsQ0FBUCxDQURpQztZQUFuQzs7QUFJQSxvQkFBUyxVQUFULENBQW9CLFFBQXBCLEVBUm9CO1VBQXRCOztBQVdBLGFBQU0sV0FBVyx5QkFBVSxNQUFNLEdBQU4sQ0FBVixDQUFYLENBZnNCO0FBZ0I1QixhQUFNLFlBQVksT0FBTyxRQUFQLElBQW9CLE9BQU8sUUFBUCxLQUFvQixFQUFwQixDQWhCVjtBQWlCNUIsa0JBQVMsUUFBVCxDQUFrQixRQUFsQixFQUE0QixVQUFVLE1BQVYsQ0FBNUIsQ0FqQjRCO0FBa0I1QixtQkFBVSxJQUFWLENBQWUsUUFBZixFQWxCNEI7UUFBUCxDQUF2QixDQUgwQzs7OztvQ0F5QjdCLFVBQVU7QUFDdkIsWUFBSyxLQUFMLENBQVcsUUFBWCxFQUFxQixVQUFDLEtBQUQsRUFBUSxTQUFSLEVBQXNCO0FBQ3pDLGFBQU0sUUFBUSxTQUFTLFFBQVQsQ0FBa0IsS0FBbEIsQ0FBUixDQURtQztBQUV6QyxhQUFJLFFBQVEsQ0FBQyxDQUFELEVBQUk7QUFDZCxxQkFBVSxNQUFWLENBQWlCLEtBQWpCLEVBQXdCLENBQXhCLEVBRGM7VUFBaEI7UUFGbUIsRUFLbEIsSUFMSCxFQUR1Qjs7OztVQTNFTjs7OztBQW9GcEIsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7bUJDckZjLFVBQUMsS0FBRCxFQUFXO0FBQ3hCLE9BQU0sY0FBYyxvREFBZCxDQURrQjs7QUFHeEIsT0FBSSxVQUFVLElBQVYsRUFBZ0I7QUFDbEIsWUFBTyxNQUFQLENBRGtCO0lBQXBCOztBQUlBLE9BQUksU0FBUyxXQUFULEVBQXNCO0FBQ3hCLFlBQU8sV0FBUCxDQUR3QjtJQUExQjs7QUFJQSxPQUFJLE1BQU0sT0FBTixDQUFjLEtBQWQsS0FBd0IsU0FBUyxRQUFULEVBQW1CO0FBQzdDLFlBQU8sS0FBSyxTQUFMLENBQWUsS0FBZixDQUFQLENBRDZDO0lBQS9DOztBQUlBLFVBQU8sS0FBSyxLQUFMLENBZmlCO0VBQVgsQzs7Ozs7Ozs7Ozs7Ozs7OztLQ0pNO0FBRW5CLFlBRm1CLEtBRW5CLEdBQWM7MkJBRkssT0FFTDs7QUFDWixVQUFLLE9BQUwsR0FBZSxFQUFmLENBRFk7SUFBZDs7Ozs7OztnQkFGbUI7OzBCQVNkLFdBQVc7OztBQUNkLGlCQUFVLE9BQVYsQ0FBa0Isb0JBQVk7QUFDNUIsYUFBSSxNQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLFFBQXJCLE1BQW1DLENBQUMsQ0FBRCxFQUFJO0FBQ3pDLGlCQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLFFBQWxCLEVBRHlDO1VBQTNDO1FBRGdCLEVBSWYsSUFKSCxFQURjO0FBTWQsY0FBTyxJQUFQLENBTmM7Ozs7K0JBU047QUFDUixjQUFRLEtBQUssSUFBTCxPQUFnQixDQUFoQixDQURBOzs7OzRCQUlIO0FBQ0wsY0FBTyxLQUFLLE9BQUwsQ0FBYSxNQUFiLENBREY7Ozs7d0JBSUosT0FBTztBQUNSLFdBQU0sTUFBTSxLQUFLLE9BQUwsQ0FBYSxLQUFiLENBQU4sQ0FERTtBQUVSLGNBQVEsT0FBTyxJQUFJLFFBQUosQ0FGUDs7Ozs2QkFLRjtBQUNOLGNBQU8sS0FBSyxFQUFMLENBQVEsQ0FBUixDQUFQLENBRE07Ozs7NEJBSUQ7QUFDTCxjQUFPLEtBQUssRUFBTCxDQUFRLEtBQUssSUFBTCxLQUFjLENBQWQsQ0FBZixDQURLOzs7O3lCQUlILFVBQVU7QUFDWixXQUFJLE9BQU8sUUFBUCxLQUFvQixVQUFwQixFQUFnQztBQUNsQyxnQkFBTyxLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCO2tCQUFPLFNBQVMsSUFBSSxRQUFKO1VBQWhCLENBQTVCLENBRGtDO1FBQXBDO0FBR0EsY0FBTyxLQUFLLE9BQUwsQ0FBYSxHQUFiLENBQWlCO2dCQUFPLElBQUksUUFBSjtRQUFQLENBQXhCLENBSlk7Ozs7MEJBT1QsWUFBWTtBQUNmLFlBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsVUFBbEIsRUFEZTtBQUVmLGNBQU8sSUFBUCxDQUZlOzs7O1VBOUNFOzs7O0FBbURwQixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7S0NwRG9COzs7Ozs7Ozs7Ozs7Ozt5QkFPUixPQUFPO0FBQ2hCLGNBQU8sT0FBTyxJQUFQLENBQVksS0FBWixFQUFtQixNQUFuQixDQUEwQixVQUFDLE1BQUQsRUFBUyxHQUFULEVBQWlCO0FBQ2hELGdCQUFPLEdBQVAsSUFBYyxPQUFPLFdBQVAsQ0FBbUIsTUFBTSxHQUFOLENBQW5CLENBQWQsQ0FEZ0Q7QUFFaEQsZ0JBQU8sTUFBUCxDQUZnRDtRQUFqQixFQUc5QixFQUhJLENBQVAsQ0FEZ0I7Ozs7Ozs7Ozs7O2lDQVlDLE9BQU87QUFDeEIsV0FBTSxjQUFjLG9EQUFkLENBRGtCO0FBRXhCLFdBQUksU0FBUyxVQUFULEVBQXFCO0FBQ3ZCLGdCQUFPLElBQUksS0FBSixFQUFQLENBRHVCO1FBQXpCOztBQUlBLFdBQUksU0FBUyxRQUFULElBQXFCLEtBQXJCLElBQThCLE1BQU0sT0FBTixDQUFjLEtBQWQsQ0FBOUIsRUFBb0Q7QUFDdEQsZ0JBQU8sS0FBSyxLQUFMLENBQVcsS0FBSyxTQUFMLENBQWUsS0FBZixDQUFYLENBQVAsQ0FEc0Q7UUFBeEQ7QUFHQSxjQUFPLEtBQVAsQ0FUd0I7Ozs7VUFuQlA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tDR0E7QUFFbkIsWUFGbUIsUUFFbkIsR0FBYzsyQkFGSyxVQUVMOztBQUNaLFVBQUssSUFBTCxHQUFZLElBQVosQ0FEWTtBQUVaLFVBQUssUUFBTCxHQUFnQixJQUFoQixDQUZZO0FBR1osVUFBSyxRQUFMLEdBQWdCLElBQWhCLENBSFk7QUFJWixVQUFLLFVBQUwsR0FBa0IsRUFBbEIsQ0FKWTtJQUFkOzs7Ozs7OztnQkFGbUI7OzhCQWFWLEtBQUs7QUFDWixjQUFPLEtBQUssVUFBTCxDQUFnQixHQUFoQixDQUFQLENBRFk7Ozs7Ozs7Ozs7OEJBUUwsS0FBSyxPQUFPO0FBQ25CLFlBQUssVUFBTCxDQUFnQixHQUFoQixJQUF1QixLQUF2QixDQURtQjs7Ozs7Ozs7OztnQ0FRVixLQUFLO0FBQ2QsY0FBTyxLQUFLLFVBQUwsQ0FBZ0IsR0FBaEIsQ0FBUCxDQURjOzs7O1VBN0JHOzs7O0FBaUNwQixFOzs7Ozs7Ozs7Ozs7Ozs7QUNwQ0Q7Ozs7Ozs7Ozs7QUFFTyxLQUFNLGtDQUFhLEVBQWI7Ozs7Ozs7S0FNUTs7Ozs7Ozs7OzRCQU1MLE1BQU07QUFDbEIsV0FBSSxXQUFXLElBQVgsQ0FBSixFQUFzQjtBQUNwQixnQkFBTyxXQUFXLElBQVgsQ0FBUCxDQURvQjtRQUF0QjtBQUdBLGNBQU8sV0FBVyxJQUFYLElBQW1CLElBQUksU0FBSixDQUFjLElBQWQsQ0FBbkIsQ0FKVzs7Ozs7Ozs7O0FBVXBCLFlBaEJtQixTQWdCbkIsQ0FBWSxJQUFaLEVBQWtCOzJCQWhCQyxXQWdCRDs7QUFDaEIsU0FBSSxDQUFDLElBQUQsRUFBTztBQUNULGFBQU0sSUFBSSxTQUFKLENBQWMsZ0NBQWQsQ0FBTixDQURTO01BQVg7O0FBSUEsVUFBSyxJQUFMLEdBQVksSUFBWixDQUxnQjtBQU1oQixVQUFLLElBQUwsR0FBWSxJQUFaLENBTmdCO0FBT2hCLFVBQUssU0FBTCxHQUFpQixFQUFqQixDQVBnQjtBQVFoQixVQUFLLFdBQUwsR0FBbUIsU0FBbkIsQ0FSZ0I7QUFTaEIsVUFBSyxZQUFMLEdBQW9CLElBQXBCLENBVGdCO0lBQWxCOzs7Ozs7Ozs7OztnQkFoQm1COzs4QkFtQ1YsTUFBTSxJQUFJLE1BQU07V0FDZixZQUFjLEtBQWQsVUFEZTs7QUFFdkIsV0FBSSxVQUFVLElBQVYsQ0FBSixFQUFxQjtBQUNuQixnQkFBTyxVQUFVLElBQVYsQ0FBUCxDQURtQjtRQUFyQjs7QUFJQSxjQUFRLDBCQUFSLENBTnVCO0FBT3ZCLFlBQU0sTUFBTSxLQUFLLFdBQUwsQ0FQVzs7QUFTdkIsV0FBTSxXQUFXLElBQUksSUFBSixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLElBQW5CLENBQVgsQ0FUaUI7V0FVZixPQUF1QixLQUF2QixLQVZlO1dBVVQsZUFBaUIsS0FBakIsYUFWUzs7O0FBWXZCLFdBQUksSUFBSixFQUFVO0FBQ1Isa0JBQVMsS0FBVCxvQ0FBa0IsS0FBbEIsRUFEUTtRQUFWOztBQUlBLFdBQUksWUFBSixFQUFrQjtBQUNoQixrQkFBUyxJQUFULENBQWMsWUFBZCxFQURnQjtRQUFsQjs7QUFJQSxjQUFPLFVBQVUsSUFBVixJQUFrQixRQUFsQixDQXBCZ0I7Ozs7Ozs7Ozs7O3dCQTRCdEIsYUFBYTtBQUNkLFdBQUksZUFBZSxDQUFDLEtBQUssV0FBTCxFQUFrQjtBQUNwQyxjQUFLLFdBQUwsR0FBbUIsV0FBbkIsQ0FEb0M7QUFFcEMsZ0JBQU8sSUFBUCxDQUZvQztRQUF0QyxNQUdPO0FBQ0wsZ0JBQU8sS0FBSyxXQUFMLENBREY7UUFIUDs7Ozs7Ozs7Ozs7NkJBYWE7eUNBQU47O1FBQU07O0FBQ2IsWUFBSyxJQUFMLEdBQVksSUFBWixDQURhO0FBRWIsY0FBTyxJQUFQLENBRmE7Ozs7Ozs7Ozs7OzBCQVVWLE9BQU07QUFDVCxXQUFJLFNBQVEsQ0FBQyxLQUFLLFlBQUwsRUFBbUI7QUFDOUIsY0FBSyxZQUFMLEdBQW9CLEtBQXBCLENBRDhCO0FBRTlCLGdCQUFPLElBQVAsQ0FGOEI7UUFBaEMsTUFHTztBQUNMLGdCQUFPLEtBQUssWUFBTCxDQURGO1FBSFA7Ozs7VUF4RmlCIiwiZmlsZSI6InN0b3JlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoW10sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wic2t0d3J4X3N0b3JlXCJdID0gZmFjdG9yeSgpO1xuXHRlbHNlXG5cdFx0cm9vdFtcInNrdHdyeF9zdG9yZVwiXSA9IGZhY3RvcnkoKTtcbn0pKHRoaXMsIGZ1bmN0aW9uKCkge1xucmV0dXJuIFxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIHdlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvblxuICoqLyIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRleHBvcnRzOiB7fSxcbiBcdFx0XHRpZDogbW9kdWxlSWQsXG4gXHRcdFx0bG9hZGVkOiBmYWxzZVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sb2FkZWQgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogd2VicGFjay9ib290c3RyYXAgNTQwOTI4MzVlZmFjZDVmY2Q2ZmRcbiAqKi8iLCJpbXBvcnQgSW5zdGFuY2UgZnJvbSAnLi9JbnN0YW5jZSc7XG5pbXBvcnQgUmVzb3VyY2UgZnJvbSAnLi9SZXNvdXJjZSc7XG5pbXBvcnQgTmFtZXNwYWNlIGZyb20gJy4vTmFtZXNwYWNlJztcblxuaW1wb3J0IHsgQ1JFQVRFLCBDSEFOR0UsIFVQREFURSwgUkVNT1ZFLCBSRVNFVCB9IGZyb20gJy4vRXZlbnRlZCc7XG5cbmV4cG9ydCBjb25zdCBuYW1lc3BhY2UgPSAobmFtZXNwYWNlKSA9PiBOYW1lc3BhY2UuY3JlYXRlKG5hbWVzcGFjZSk7XG5cbmV4cG9ydCBjb25zdCByZXNvdXJjZSA9IChuYW1lLCBpZEF0dHIpID0+IG5ldyBSZXNvdXJjZShuYW1lLCBpZEF0dHIpO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIFJlc291cmNlLFxuICBJbnN0YW5jZSxcbiAgTmFtZXNwYWNlLFxuICBldmVudHM6IHsgQ1JFQVRFLCBDSEFOR0UsIFVQREFURSwgUkVNT1ZFLCBSRVNFVCB9XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9saWIvbWFpbi5qc1xuICoqLyIsImltcG9ydCBFdmVudGVkLCB7IENIQU5HRSwgVVBEQVRFIH0gZnJvbSAnLi9FdmVudGVkJztcblxuLyoqXG4gKiBAcHVibGljXG4gKiBAY2xhc3MgSW5zdGFuY2VcbiAqIEBleHRlbmRzIHtFdmVudGVkfVxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJbnN0YW5jZSBleHRlbmRzIEV2ZW50ZWQge1xuXG4gIC8qKlxuICAgKlxuICAgKiBAcGFyYW0ge1Jlc291cmNlfSByZXNvdXJjZVxuICAgKiBAcGFyYW0ge29iamVjdH0gcHJvcHNcbiAgICogQHBhcmFtIHtQb3NpdGlvbn0gcG9zaXRpb25cbiAgICAgKi9cbiAgY29uc3RydWN0b3IocmVzb3VyY2UsIHByb3BzID0ge30sIHBvc2l0aW9uKSB7XG4gICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcblxuICAgIHRoaXMucmVzb3VyY2UgPSByZXNvdXJjZTtcbiAgICB0aGlzLnBvc2l0aW9uID0gcG9zaXRpb247XG4gICAgdGhpcy5wcm9wcyA9IHByb3BzO1xuICAgIHRoaXMuY2hhbmdlcyA9IHt9O1xuXG4gICAgdGhpcy5yZWdpc3RlckV2ZW50cyhDSEFOR0UsIFVQREFURSk7XG4gIH1cblxuICBnZXRQcm9wcygpIHtcbiAgICBjb25zdCBib2R5ICA9IHRoaXMucmVzb3VyY2UuYm9keSgpO1xuICAgIGlmIChib2R5KSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9wc1tib2R5XTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMucHJvcHM7XG4gIH1cblxuICAvKipcbiAgICogR2V0IG5leHQgSW5zdGFuY2UgaW4gdGhlIGNoYWluXG4gICAqIEByZXR1cm5zIHtJbnN0YW5jZXxudWxsfVxuICAgKi9cbiAgbmV4dCgpIHtcbiAgICBjb25zdCBuZXh0ID0gdGhpcy5wb3NpdGlvbi5uZXh0O1xuICAgIHJldHVybiAobmV4dCAmJiBuZXh0Lmluc3RhbmNlIHx8IG51bGwpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBwcmV2aW91cyBJbnN0YW5jZSBpbiB0aGUgY2hhaW5cbiAgICogQHJldHVybnMge0luc3RhbmNlfG51bGx9XG4gICAqL1xuICBwcmV2aW91cygpIHtcbiAgICBjb25zdCBwcmV2aW91cyA9IHRoaXMucG9zaXRpb24ucHJldmlvdXM7XG4gICAgcmV0dXJuIChwcmV2aW91cyAmJiBwcmV2aW91cy5pbnN0YW5jZSB8fCBudWxsKTtcbiAgfVxuXG4gIGdldChrZXkpIHtcbiAgICBjb25zdCBib2R5ID0gdGhpcy5nZXRQcm9wcygpO1xuICAgIHJldHVybiBib2R5W2tleV07XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJuIHRoZSBmaWVsZCBzcGVjaWZpZWQgYnkgUmVzb3VyY2UjYm9keSgpXG4gICAqIEByZXR1cm5zIHtzdHJpbmd8bnVsbH1cbiAgICovXG4gIGlkKCkge1xuICAgIGNvbnN0IHsgaWRBdHRyaWJ1dGUgfSA9IHRoaXMucmVzb3VyY2U7XG4gICAgaWYgKGlkQXR0cmlidXRlKSB7XG4gICAgICByZXR1cm4gdGhpcy5nZXQoaWRBdHRyaWJ1dGUpO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm4gdGhlIGNoYW5nZWQgdmFsdWUgdG8gYSBmaWVsZFxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gICAqIEByZXR1cm5zIHsqfVxuICAgKi9cbiAgZ2V0Q2hhbmdlKGtleSkge1xuICAgIHJldHVybiB0aGlzLmNoYW5nZXNba2V5XTtcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGUgYSBmaWVsZCdzIHZhbHVlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoXG4gICAqXG4gICAqIEBleGFtcGxlXG4gICAqXG4gICAqIEluc3RhbmNlI3Jlc29sdmUoJ2EuYi5jJyk7XG4gICAqXG4gICAqIEByZXR1cm5zIHsqfVxuICAgKi9cbiAgcmVzb2x2ZShwYXRoID0gJycpIHtcbiAgICBwYXRoID0gKHBhdGggJiYgcGF0aC5zcGxpdCgnLicpIHx8IFtdKTtcbiAgICBsZXQgcHJvcHMgPSB0aGlzLnByb3BzO1xuXG4gICAgd2hpbGUgKHBhdGgubGVuZ3RoID4gMCkge1xuICAgICAgY29uc3QgZmllbGQgPSBwYXRoLnNoaWZ0KCk7XG4gICAgICBpZiAoIXByb3BzW2ZpZWxkXSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBwcm9wcyA9IHByb3BzW2ZpZWxkXTtcbiAgICB9XG5cbiAgICByZXR1cm4gcHJvcHM7XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlIGEgYm9keSBmaWVsZCdzIHZhbHVlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBmaWVsZFxuICAgKiBAcGFyYW0geyp9IHZhbHVlXG4gICAqIEByZXR1cm5zIHtJbnN0YW5jZX1cbiAgICovXG4gIHNldChmaWVsZCwgdmFsdWUpIHtcbiAgICB0aGlzLmNoYW5nZSh7IFtmaWVsZF06IHZhbHVlIH0pO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBjaGFuZ2VzXG4gICAqL1xuICBjaGFuZ2UoY2hhbmdlcykge1xuICAgIGNvbnN0IG9sZENoYW5nZXMgPSB0aGlzLmNoYW5nZXM7XG4gICAgdGhpcy5jaGFuZ2VzID0gT2JqZWN0LmFzc2lnbih7fSwgb2xkQ2hhbmdlcywgY2hhbmdlcyk7XG4gICAgdGhpcy5wdWJsaXNoKENIQU5HRSwgdGhpcywgb2xkQ2hhbmdlcywgY2hhbmdlcyk7XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlIHRoZSByZXNvdXJjZSBJbnN0YW5jZVxuICAgKiBAcGFyYW0ge29iamVjdH0gY2hhbmdlcyBmaWVsZHMocykgLSB2YWx1ZShzKVxuICAgKi9cbiAgdXBkYXRlKGNoYW5nZXMpIHtcbiAgICBjb25zdCBkZWx0YSA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuY2hhbmdlcywgY2hhbmdlcyk7XG4gICAgY29uc3Qgb2xkUHJvcHMgPSB0aGlzLmdldFByb3BzKCk7XG4gICAgY29uc3QgcHJvcHMgPSBPYmplY3QuYXNzaWduKHt9LCBvbGRQcm9wcywgZGVsdGEpO1xuXG4gICAgLy8gaWYgdGhlcmUgYXJlIGFueSBhY3R1YWwgY2hhbmdlc1xuICAgIGlmIChKU09OLnN0cmluZ2lmeShwcm9wcykgIT09IEpTT04uc3RyaW5naWZ5KHRoaXMucHJvcHMpKSB7XG4gICAgICB0aGlzLmNoYW5nZXMgPSB7fTtcbiAgICAgIHRoaXMucHJvcHMgPSBwcm9wcztcblxuICAgICAgdGhpcy5wdWJsaXNoKFVQREFURSwgdGhpcywgb2xkUHJvcHMsIGRlbHRhKTtcbiAgICB9XG4gIH1cblxuICByZW1vdmUoKSB7XG4gICAgdGhpcy5yZXNvdXJjZS5yZW1vdmUodGhpcyk7XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybnMge29iamVjdH1cbiAgICovXG4gIHRvSlNPTigpIHtcbiAgICByZXR1cm4gSlNPTi5wYXJzZSh0aGlzLnRvU3RyaW5nKCkpO1xuICB9XG5cbiAgLyoqXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAqL1xuICB0b1N0cmluZygpIHtcbiAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9wcyk7XG4gIH1cbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2xpYi9JbnN0YW5jZS5qc1xuICoqLyIsImV4cG9ydCBjb25zdCBDUkVBVEUgPSAnY3JlYXRlJztcbmV4cG9ydCBjb25zdCBDSEFOR0UgPSAnY2hhbmdlJztcbmV4cG9ydCBjb25zdCBVUERBVEUgPSAndXBkYXRlJztcbmV4cG9ydCBjb25zdCBSRU1PVkUgPSAncmVtb3ZlJztcbmV4cG9ydCBjb25zdCBSRVNFVCA9ICdyZXNldCc7XG5cbi8qKlxuICogQHByb3RlY3RlZFxuICogQGNsYXNzIEV2ZW50ZWRcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXZlbnRlZCB7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5saXN0ZW5lcnMgPSB7fTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZVxuICAgKi9cbiAgcmVnaXN0ZXJFdmVudCh0eXBlKSB7XG4gICAgdGhpcy5saXN0ZW5lcnNbdHlwZV0gPSAodGhpcy5saXN0ZW5lcnNbdHlwZV0gfHwgW10pO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nW119IHR5cGVzXG4gICAqL1xuICByZWdpc3RlckV2ZW50cyguLi50eXBlcykge1xuICAgIHR5cGVzLmZvckVhY2godGhpcy5yZWdpc3RlckV2ZW50LCB0aGlzKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZVxuICAgKiBAcGFyYW0geypbXX0gcGF5bG9hZFxuICAgKiBAcmV0dXJucyB7RXZlbnRlZH1cbiAgICovXG4gIHB1Ymxpc2godHlwZSwgLi4ucGF5bG9hZCkge1xuICAgIGlmICghdGhpcy5saXN0ZW5lcnNbdHlwZV0pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihgJHt0aGlzLmNvbnN0cnVjdG9yLm5hbWV9ICcke3R5cGV9JyBpcyBub3QgYSByZWdpc3RlcmVkIGV2ZW50IWApO1xuICAgIH1cbiAgICB0aGlzLmxpc3RlbmVyc1t0eXBlXS5mb3JFYWNoKGxpc3RlbmVyID0+IGxpc3RlbmVyKC4uLnBheWxvYWQpKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZVxuICAgKiBAcGFyYW0geypbXX0gcGF5bG9hZFxuICAgKiBAcmV0dXJucyB7RXZlbnRlZH1cbiAgICovXG4gIGFzeW5jUHVibGlzaCh0eXBlLCAuLi5wYXlsb2FkKSB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnB1Ymxpc2godHlwZSwgLi4ucGF5bG9hZCksIDApO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlXG4gICAqIEBwYXJhbSB7ZnVuY3Rpb259IGxpc3RlbmVyXG4gICAqIEByZXR1cm5zIHtFdmVudGVkfVxuICAgKi9cbiAgc3Vic2NyaWJlKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgaWYgKHR5cGVvZiBsaXN0ZW5lciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IGxpc3RlbmVycyA9IHRoaXMubGlzdGVuZXJzW3R5cGVdO1xuICAgIGlmIChsaXN0ZW5lcnMgJiYgbGlzdGVuZXJzLmluZGV4T2YobGlzdGVuZXIpID09PSAtMSkge1xuICAgICAgbGlzdGVuZXJzLnB1c2gobGlzdGVuZXIpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZVxuICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBsaXN0ZW5lclxuICAgKiBAcmV0dXJucyB7RXZlbnRlZH1cbiAgICovXG4gIHVuc3Vic2NyaWJlKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgY29uc3QgbGlzdGVuZXJzID0gdGhpcy5saXN0ZW5lcnNbdHlwZV07XG4gICAgaWYgKGxpc3RlbmVycykge1xuICAgICAgaWYgKHR5cGVvZiBsaXN0ZW5lciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLmxpc3RlbmVyc1t0eXBlXSA9IFtdO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBpbmRleCA9IGxpc3RlbmVycy5pbmRleE9mKGxpc3RlbmVyKTtcbiAgICAgIGlmIChpbmRleCA+IC0xKSB7XG4gICAgICAgIGxpc3RlbmVycy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIHJlc2V0RXZlbnRzKCkge1xuICAgIGNvbnN0IHsgbGlzdGVuZXJzIH0gPSB0aGlzO1xuICAgIHRoaXMubGlzdGVuZXJzID0gT2JqZWN0LmtleXMobGlzdGVuZXJzKS5yZWR1Y2UoKGxpc3RlbmVycywgdHlwZSkgPT4ge1xuICAgICAgbGlzdGVuZXJzW3R5cGVdID0gW107XG4gICAgICByZXR1cm4gbGlzdGVuZXJzO1xuICAgIH0sIHt9KTtcbiAgfVxuXG59XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2xpYi9FdmVudGVkLmpzXG4gKiovIiwiaW1wb3J0IEluZGV4IGZyb20gJy4vSW5kZXgnO1xuaW1wb3J0IFF1ZXJ5IGZyb20gJy4vUXVlcnknO1xuaW1wb3J0IFNjaGVtYSBmcm9tICcuL1NjaGVtYSc7XG5pbXBvcnQgUG9zaXRpb24gZnJvbSAnLi9Qb3NpdGlvbic7XG5pbXBvcnQgSW5zdGFuY2UgZnJvbSAnLi9JbnN0YW5jZSc7XG5pbXBvcnQgRXZlbnRlZCwgeyBDUkVBVEUsIENIQU5HRSwgVVBEQVRFLCBSRU1PVkUsIFJFU0VUIH0gZnJvbSAnLi9FdmVudGVkJztcblxuLyoqXG4gKiBAY2xhc3MgUmVzb3VyY2VcbiAqIEBleHRlbmRzIHtFdmVudGVkfVxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXNvdXJjZSBleHRlbmRzIEV2ZW50ZWQge1xuXG4gIGNvbnN0cnVjdG9yKG5hbWUgPSAnJywgaWRBdHRyaWJ1dGUgPSAnaWQnLCBuYW1lc3BhY2UgPSBudWxsKSB7XG4gICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcblxuICAgIHRoaXMuaWRBdHRyaWJ1dGUgPSBpZEF0dHJpYnV0ZTtcbiAgICB0aGlzLm5hbWVzcGFjZSA9IG5hbWVzcGFjZTtcbiAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgIHRoaXMub3JkZXIgPSBbXTtcbiAgICB0aGlzLnJlc291cmNlQm9keSA9IG51bGw7XG4gICAgdGhpcy5JbnN0YW5jZSA9IEluc3RhbmNlO1xuICAgIHRoaXMuX3Byb3BzID0ge307XG4gICAgdGhpcy5fc2NoZW1hID0ge307XG4gICAgdGhpcy5faW5kZXggPSBuZXcgSW5kZXgodGhpcyk7XG5cbiAgICBpZiAoaWRBdHRyaWJ1dGUpIHtcbiAgICAgIHRoaXMuaW5kZXgoaWRBdHRyaWJ1dGUpO1xuICAgIH1cbiAgICBcbiAgICB0aGlzLnJlZ2lzdGVyRXZlbnRzKENSRUFURSwgQ0hBTkdFLCBVUERBVEUsIFJFTU9WRSwgUkVTRVQpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7UmVzb3VyY2V9IHBvc2l0aW9uXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBvbGRQcm9wcyBhbGwgb2xkIHByb3BzXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBkZWx0YSBjaGFuZ2VkIGZpZWxkIHdpdGggbmV3IHZhbHVlc1xuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gIF9yZWluZGV4KHsgcG9zaXRpb24gfSwgb2xkUHJvcHMsIGRlbHRhKSB7XG4gICAgdGhpcy5faW5kZXgucmVpbmRleFBvc2l0aW9uKHBvc2l0aW9uLCBvbGRQcm9wcywgZGVsdGEpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBwcm9wc1xuICAgKiBAcGFyYW0ge251bWJlcn0gW2luZGV4XVxuICAgKiBAcmV0dXJucyB7SW5zdGFuY2V8SW5zdGFuY2VbXX1cbiAgICovXG4gIF9jcmVhdGUocHJvcHMsIGluZGV4KSB7XG4gICAgaWYgKHR5cGVvZiBpbmRleCAhPT0gJ251bWJlcicgfHwgaXNOYU4oaW5kZXgpKSB7XG4gICAgICBpbmRleCA9IHRoaXMub3JkZXIubGVuZ3RoO1xuICAgIH1cblxuICAgIGNvbnN0IHBvc2l0aW9uID0gbmV3IFBvc2l0aW9uKCk7XG4gICAgY29uc3QgcHJldmlvdXMgPSAodGhpcy5vcmRlcltpbmRleCAtIDFdIHx8IG51bGwpO1xuICAgIGlmIChwcmV2aW91cykge1xuICAgICAgcHJldmlvdXMubmV4dCA9IHBvc2l0aW9uO1xuICAgIH1cbiAgICBjb25zdCBuZXh0ID0gKHRoaXMub3JkZXJbaW5kZXhdIHx8IG51bGwpO1xuICAgIGlmIChuZXh0KSB7XG4gICAgICBuZXh0LnByZXZpb3VzID0gcG9zaXRpb247XG4gICAgfVxuICAgIHBvc2l0aW9uLm5leHQgPSBuZXh0O1xuICAgIHBvc2l0aW9uLnByZXZpb3VzID0gcHJldmlvdXM7XG5cbiAgICBjb25zdCBzY2hlbWEgPSBTY2hlbWEuZ2V0KHRoaXMuX3NjaGVtYSk7XG4gICAgY29uc3QgYWxsUHJvcHMgPSBPYmplY3QuYXNzaWduKHt9LCBzY2hlbWEsIHByb3BzKTtcbiAgICBjb25zdCB7IEluc3RhbmNlIH0gPSB0aGlzO1xuICAgIGNvbnN0IGluc3RhbmNlID0gbmV3IEluc3RhbmNlKHRoaXMsIGFsbFByb3BzLCBwb3NpdGlvbik7XG4gICAgcG9zaXRpb24uaW5zdGFuY2UgPSBpbnN0YW5jZTtcblxuICAgIHRoaXMuX2luZGV4LmluZGV4UG9zaXRpb24ocG9zaXRpb24pO1xuICAgIHRoaXMub3JkZXIuc3BsaWNlKGluZGV4LCAwLCBwb3NpdGlvbik7XG5cbiAgICBpbnN0YW5jZS5zdWJzY3JpYmUoVVBEQVRFLCAoaW5zdGFuY2UsIG9sZFByb3BzLCBkZWx0YSkgPT4ge1xuICAgICAgdGhpcy5faW5kZXgucmVpbmRleFBvc2l0aW9uKGluc3RhbmNlLnBvc2l0aW9uLCBvbGRQcm9wcywgZGVsdGEpO1xuICAgICAgdGhpcy5wdWJsaXNoKFVQREFURSwgaW5zdGFuY2UsIG9sZFByb3BzLCBkZWx0YSk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnB1Ymxpc2goQ1JFQVRFLCBpbnN0YW5jZSwgaW5kZXgpO1xuXG4gICAgcmV0dXJuIGluc3RhbmNlO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBwcm9wc1xuICAgKiBAcGFyYW0ge251bWJlcn0gW2luZGV4XVxuICAgKiBAcmV0dXJucyB7SW5zdGFuY2V8SW5zdGFuY2VbXX1cbiAgICovXG4gIF9zdG9yZShwcm9wcywgaW5kZXgpIHtcbiAgICBjb25zdCBpZCA9IHRoaXMuaWRBdHRyaWJ1dGU7XG4gICAgbGV0IHF1ZXJ5ID0gdGhpcy5maW5kKGlkLCBwcm9wc1tpZF0pO1xuICAgIGlmICghcXVlcnkuaXNFbXB0eSgpKSB7XG4gICAgICBjb25zdCBpbnN0YW5jZSA9IHF1ZXJ5LmZpcnN0KCk7XG4gICAgICBpbnN0YW5jZS51cGRhdGUocHJvcHMpO1xuICAgICAgcmV0dXJuIGluc3RhbmNlO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLl9jcmVhdGUocHJvcHMsIGluZGV4KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBrZXlzIG9mIGluc3RhbmNlcyB0byBpbmRleFxuICAgKiBAcGFyYW0ge3N0cmluZ1tdfSBrZXlzXG4gICAqIEByZXR1cm5zIHtSZXNvdXJjZX1cbiAgICAgKi9cbiAgaW5kZXgoLi4ua2V5cykge1xuICAgIGNvbnN0IHsgX2luZGV4IH0gPSB0aGlzO1xuICAgIGtleXMuZm9yRWFjaChfaW5kZXguaW5kZXhLZXksIF9pbmRleCk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICAvKipcbiAgICogRGVmYXVsdCBrZXlzIHdpdGggZGVmYXVsdCB2YWx1ZXNcbiAgICogQHBhcmFtIHtvYmplY3R9IGRlZmF1bHRzXG4gICAqIEByZXR1cm5zIHtSZXNvdXJjZX1cbiAgICovXG4gIGRlZmF1bHRzKGRlZmF1bHRzKSB7XG4gICAgdGhpcy5fc2NoZW1hID0gZGVmYXVsdHM7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICAvKipcbiAgICogU2V0IHRoZSBJbnN0YW5jZSBjbGFzc1xuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBJbnN0YW5jZVxuICAgKi9cbiAgaW5zdGFuY2UoSW5zdGFuY2UpIHtcbiAgICB0aGlzLkluc3RhbmNlID0gSW5zdGFuY2U7XG4gIH1cblxuICAvKipcbiAgICogU3BlY2lmeSB0aGUgbmFtZXNwYWNlIHRvIGluZGV4IGZvciBhbiBJbnN0YW5jZVxuICAgKiBUaGlzIGNhbiBiZSBzcGVjaWZpZWQgb25jZSBvbmx5IVxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gICAqIEByZXR1cm5zIHsqfVxuICAgKi9cbiAgYm9keShrZXkpIHtcbiAgICBpZiAoa2V5ICYmICF0aGlzLnJlc291cmNlQm9keSkge1xuICAgICAgdGhpcy5yZXNvdXJjZUJvZHkgPSBrZXk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMucmVzb3VyY2VCb2R5O1xuICB9XG5cbiAgZ2V0KGtleSkge1xuICAgIHJldHVybiB0aGlzLl9wcm9wc1trZXldO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBrZXlcbiAgICogQHBhcmFtIHsqfSB2YWx1ZVxuICAgKiBAcmV0dXJucyB7UmVzb3VyY2V9XG4gICAqL1xuICBzZXQoa2V5LCB2YWx1ZSkge1xuICAgIHRoaXMuY2hhbmdlKHsgW2tleV06IHZhbHVlIH0pO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBwcm9wc1xuICAgKiBAcmV0dXJucyB7UmVzb3VyY2V9XG4gICAqL1xuICBjaGFuZ2UocHJvcHMpIHtcbiAgICBjb25zdCB7IF9wcm9wcyB9ID0gdGhpcy5fcHJvcHM7XG4gICAgcHJvcHMgPSBPYmplY3QuYXNzaWduKHt9LCBfcHJvcHMsIHByb3BzKTtcbiAgICB0aGlzLl9wcm9wcyA9IHByb3BzO1xuICAgIHRoaXMucHVibGlzaChDSEFOR0UsIHByb3BzKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge29iamVjdHxvYmplY3RbXX0gcHJvcHNcbiAgICogQHBhcmFtIHtudW1iZXJ9IFtpbmRleF1cbiAgICogQHJldHVybnMge1Jlc291cmNlfFJlc291cmNlW119XG4gICAqL1xuICBzdG9yZShwcm9wcywgaW5kZXgpIHtcbiAgICBpZiAoQXJyYXkuaXNBcnJheShwcm9wcykpIHtcbiAgICAgIHJldHVybiBwcm9wcy5tYXAoKHByb3AsIGkpID0+IHtcbiAgICAgICAgY29uc3QgYXQgID0gKHR5cGVvZiBpbmRleCA9PT0gJ251bWJlcicgPyBpbmRleCArIGkgOiB1bmRlZmluZWQpO1xuICAgICAgICByZXR1cm4gdGhpcy5fc3RvcmUocHJvcCwgYXQpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzLl9zdG9yZShwcm9wcywgaW5kZXgpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm4gcG9zaXRpb25zIGZvciBhIGtleSBhbmQgYSB2YWx1ZVxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gICAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAgICogQHJldHVybnMge251bWJlcltdfG51bWJlcn1cbiAgICovXG4gIHBvc2l0aW9uKGtleSwgdmFsdWUpIHtcbiAgICBjb25zdCBwb3NpdGlvbnMgPSB0aGlzLl9pbmRleC5nZXQoa2V5LCB2YWx1ZSk7XG4gICAgcmV0dXJuIChwb3NpdGlvbnMgPyBwb3NpdGlvbnMubWFwKHBvcyA9PiB0aGlzLm9yZGVyLmluZGV4T2YocG9zKSkgOiAtMSk7XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybnMge0luc3RhbmNlfG51bGx9XG4gICAqL1xuICBmaXJzdCgpIHtcbiAgICByZXR1cm4gdGhpcy5hdCgwKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcmV0dXJucyB7SW5zdGFuY2V8bnVsbH1cbiAgICovXG4gIGxhc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMuYXQodGhpcy5vcmRlci5sZW5ndGggLSAxKTtcbiAgfVxuXG4gIC8qKlxuICAgKlxuICAgKiBAcGFyYW0ge0luc3RhbmNlfSBpbnN0YW5jZVxuICAgKiBAcGFyYW0ge29iamVjdH0gcHJvcHNcbiAgICogQHJldHVybnMge1Jlc291cmNlfVxuICAgKi9cbiAgYmVmb3JlKGluc3RhbmNlLCBwcm9wcykge1xuICAgIGNvbnN0IGlkID0gaW5zdGFuY2UuaWQoKTtcbiAgICBjb25zdCBpbmRleCA9IHRoaXMucG9zaXRpb24odGhpcy5pZEF0dHJpYnV0ZSwgaWQpWzBdO1xuICAgIHJldHVybiB0aGlzLnN0b3JlKHByb3BzLCBpbmRleCk7XG4gIH1cbiAgXG4gIC8qKlxuICAgKlxuICAgKiBAcGFyYW0ge0luc3RhbmNlfSBpbnN0YW5jZVxuICAgKiBAcGFyYW0ge29iamVjdH0gcHJvcHNcbiAgICogQHJldHVybnMge1Jlc291cmNlfVxuICAgKi9cbiAgYWZ0ZXIoaW5zdGFuY2UsIHByb3BzKSB7XG4gICAgY29uc3QgaWQgPSBpbnN0YW5jZS5pZCgpO1xuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5wb3NpdGlvbih0aGlzLmlkQXR0cmlidXRlLCBpZClbMF07XG4gICAgcmV0dXJuIHRoaXMuc3RvcmUocHJvcHMsIGluZGV4ICsgMSk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtmdW5jdGlvbn0gZXhlY1xuICAgKiBAcmV0dXJucyB7YXJyYXl9XG4gICAqL1xuICBhbGwoZXhlYykge1xuICAgIGxldCBpdGVyYXRvciA9IHBvcyA9PiBwb3MuaW5zdGFuY2U7XG4gICAgaWYgKHR5cGVvZiBleGVjID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBpdGVyYXRvciA9IChwb3MsIC4uLmFyZ3MpID0+IGV4ZWMocG9zLmluc3RhbmNlLCAuLi5hcmdzKTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMub3JkZXIubWFwKGl0ZXJhdG9yKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcmV0dXJucyB7bnVtYmVyfVxuICAgKi9cbiAgc2l6ZSgpIHtcbiAgICByZXR1cm4gdGhpcy5vcmRlci5sZW5ndGg7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IGZpZWxkXG4gICAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAgICogQHBhcmFtIHtxdWVyeX0gW3F1ZXJ5XVxuICAgKiBAcmV0dXJucyB7UXVlcnl9XG4gICAqL1xuICBmaW5kKGZpZWxkLCB2YWx1ZSwgcXVlcnkpIHtcbiAgICBjb25zdCBwb3NpdGlvbnMgPSB0aGlzLl9pbmRleC5nZXQoZmllbGQsIHZhbHVlKTtcbiAgICBxdWVyeSA9IChxdWVyeSB8fCBuZXcgUXVlcnkoKSk7XG5cbiAgICBpZiAocG9zaXRpb25zKSB7XG4gICAgICByZXR1cm4gcXVlcnkucHVzaChwb3NpdGlvbnMpO1xuICAgIH1cblxuICAgIGxldCBwb3NpdGlvbiA9IHRoaXMub3JkZXJbMF07XG4gICAgd2hpbGUgKHBvc2l0aW9uKSB7XG4gICAgICBpZiAocG9zaXRpb24uaW5zdGFuY2UuZ2V0KGZpZWxkKSA9PT0gdmFsdWUpIHtcbiAgICAgICAgcXVlcnkucHVzaChbcG9zaXRpb25dKTtcbiAgICAgIH1cbiAgICAgIHBvc2l0aW9uID0gcG9zaXRpb24ubmV4dDtcbiAgICB9XG4gICAgXG4gICAgcmV0dXJuIHF1ZXJ5O1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBwcm9wc1xuICAgKi9cbiAgcXVlcnkocHJvcHMpIHtcbiAgICBjb25zdCBxdWVyeSA9IG5ldyBRdWVyeSgpO1xuICAgIE9iamVjdC5rZXlzKHByb3BzKS5mb3JFYWNoKGtleSA9PiB0aGlzLmZpbmQoa2V5LCBwcm9wc1trZXldLCBxdWVyeSkpO1xuICAgIHJldHVybiBxdWVyeTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gICAqIEByZXR1cm5zIHthcnJheX1cbiAgICovXG4gIHZhbHVlcyhrZXkpIHtcbiAgICByZXR1cm4gdGhpcy5faW5kZXguZ2V0VmFsdWVzRm9yKGtleSk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtudW1iZXJ9IGluZGV4XG4gICAqIEByZXR1cm5zIHtJbnN0YW5jZXxudWxsfVxuICAgKi9cbiAgYXQoaW5kZXgpIHtcbiAgICBjb25zdCBwb3NpdGlvbiA9IHRoaXMub3JkZXJbaW5kZXhdO1xuICAgIHJldHVybiAocG9zaXRpb24gJiYgcG9zaXRpb24uaW5zdGFuY2UgfHwgbnVsbCk7XG4gIH1cblxuICAvLyBjb252ZW5pZW5jZSBtZXRob2Qgb25seSAtIHVzZSBDb2xsZWN0aW9uI2ZpbmQvYXQgLT4gSW5zdGFuY2UjdXBkYXRlIG9yIENvbGxlY3Rpb24jdXBzZXJ0XG4gIHVwZGF0ZShpbnN0YW5jZSwgdXBkYXRlcykge1xuICAgIGluc3RhbmNlLnVwZGF0ZSh1cGRhdGVzKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0luc3RhbmNlfSBpbnN0YW5jZVxuICAgKi9cbiAgcmVtb3ZlKGluc3RhbmNlKSB7XG4gICAgY29uc3QgeyBwb3NpdGlvbiB9ID0gaW5zdGFuY2U7XG4gICAgY29uc3QgaW5kZXggPSB0aGlzLm9yZGVyLmluZGV4T2YocG9zaXRpb24pO1xuICAgIGlmIChpbmRleCA+IC0xKSB7XG4gICAgICB0aGlzLm9yZGVyLnNwbGljZShpbmRleCwgMSk7XG4gICAgfVxuXG4gICAgdGhpcy5faW5kZXgucmVtb3ZlUG9zaXRpb24ocG9zaXRpb24pO1xuXG4gICAgY29uc3QgeyBwcmV2aW91cywgbmV4dCB9ID0gcG9zaXRpb247XG4gICAgaWYgKHByZXZpb3VzKSB7XG4gICAgICBwcmV2aW91cy5uZXh0ID0gbmV4dDtcbiAgICB9XG4gICAgaWYgKG5leHQpIHtcbiAgICAgIG5leHQucHJldmlvdXMgPSBwcmV2aW91cztcbiAgICB9XG5cbiAgICBpbnN0YW5jZS5yZXNldEV2ZW50cygpO1xuICAgIHRoaXMucHVibGlzaChSRU1PVkUsIGluc3RhbmNlLCBpbmRleCk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHRyaWdnZXJSZW1vdmVzXG4gICAqL1xuICByZXNldCh0cmlnZ2VyUmVtb3Zlcykge1xuICAgIGlmICh0cmlnZ2VyUmVtb3ZlcyA9PT0gdHJ1ZSkge1xuICAgICAgdGhpcy5vcmRlci5mb3JFYWNoKCh7IGluc3RhbmNlIH0pID0+IHRoaXMucmVtb3ZlKGluc3RhbmNlKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMub3JkZXIuZm9yRWFjaChwb3MgPT4ge1xuICAgICAgICBwb3MuaW5zdGFuY2UucmVzZXRFdmVudHMoKTtcbiAgICAgICAgdGhpcy5faW5kZXgucmVtb3ZlUG9zaXRpb24ocG9zKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICB0aGlzLm9yZGVyID0gW107XG4gICAgdGhpcy5fcHJvcHMgPSB7fTtcbiAgICB0aGlzLnB1Ymxpc2goUkVTRVQpO1xuICB9XG5cbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2xpYi9SZXNvdXJjZS5qc1xuICoqLyIsImltcG9ydCBzdHJpbmdpZnkgZnJvbSAnLi9zdHJpbmdpZnknO1xuXG4vKipcbiAqIEBwcm90ZWN0ZWRcbiAqIEBjbGFzcyBJbmRleFxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJbmRleCB7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5sb29rdXAgPSB7fTtcbiAgICB0aGlzLmluZGV4S2V5cyA9IFtdO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfZmluZChwb3NpdGlvbiwgZXhlY3V0b3IsIGRlbGV0ZUlmRW1wdHkpIHtcbiAgICBjb25zdCBib2R5ID0gcG9zaXRpb24uaW5zdGFuY2UuZ2V0UHJvcHMoKTtcbiAgICBjb25zdCB7IGxvb2t1cCB9ID0gdGhpcztcblxuICAgIHRoaXMuaW5kZXhLZXlzLmZvckVhY2goa2V5ID0+IHtcbiAgICAgIGNvbnN0IHZhbHVlID0gc3RyaW5naWZ5KGJvZHlba2V5XSk7XG4gICAgICBjb25zdCB2YWx1ZXMgPSBsb29rdXBba2V5XSA9IChsb29rdXBba2V5XSB8fCB7fSk7XG4gICAgICBjb25zdCBwb3NpdGlvbnMgPSB2YWx1ZXNbdmFsdWVdID0gKHZhbHVlc1t2YWx1ZV0gfHwgW10pO1xuXG4gICAgICBleGVjdXRvcih2YWx1ZSwgcG9zaXRpb25zKTtcblxuICAgICAgaWYgKGRlbGV0ZUlmRW1wdHkgJiYgcG9zaXRpb25zLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICBkZWxldGUgdmFsdWVzW3ZhbHVlXTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGluZGV4S2V5KGtleSkge1xuICAgIGlmICh0aGlzLmluZGV4S2V5cy5pbmRleE9mKGtleSkgPT09IC0xKSB7XG4gICAgICB0aGlzLmluZGV4S2V5cy5wdXNoKGtleSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0VmFsdWVzRm9yKGtleSkge1xuICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmxvb2t1cFtrZXldIHx8IHt9KTtcbiAgfVxuXG4gIGdldChrZXksIHZhbHVlKSB7XG4gICAgY29uc3QgZmllbGQgPSB0aGlzLmxvb2t1cFtrZXldO1xuICAgIHZhbHVlID0gc3RyaW5naWZ5KHZhbHVlKTtcbiAgICByZXR1cm4gKGZpZWxkICYmIGZpZWxkW3ZhbHVlXSB8fCBudWxsKTtcbiAgfVxuXG4gIGluZGV4UG9zaXRpb24ocG9zaXRpb24pIHtcbiAgICB0aGlzLl9maW5kKHBvc2l0aW9uLCAodmFsdWUsIHBvc2l0aW9ucykgPT4ge1xuICAgICAgcG9zaXRpb24uc2V0SW5kZXgodmFsdWUsIHBvc2l0aW9ucy5sZW5ndGgpO1xuICAgICAgcG9zaXRpb25zLnB1c2gocG9zaXRpb24pO1xuICAgIH0pO1xuICB9XG5cbiAgcmVpbmRleFBvc2l0aW9uKHBvc2l0aW9uLCBvbGRWYWx1ZXMsIGRlbHRhKSB7XG4gICAgY29uc3QgeyBsb29rdXAgfSA9IHRoaXM7XG5cbiAgICB0aGlzLmluZGV4S2V5cy5mb3JFYWNoKGtleSA9PiB7XG4gICAgICBjb25zdCB2YWx1ZXMgPSBsb29rdXBba2V5XTtcbiAgICAgIGNvbnN0IG9sZFZhbHVlID0gc3RyaW5naWZ5KG9sZFZhbHVlc1trZXldKTtcblxuICAgICAgaWYgKHZhbHVlc1tvbGRWYWx1ZV0pIHtcbiAgICAgICAgY29uc3QgaW5kZXggPSBwb3NpdGlvbi5nZXRJbmRleChvbGRWYWx1ZSk7XG4gICAgICAgIHZhbHVlc1tvbGRWYWx1ZV0uc3BsaWNlKGluZGV4LCAxKTtcblxuICAgICAgICBpZiAodmFsdWVzW29sZFZhbHVlXS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICBkZWxldGUgdmFsdWVzW29sZFZhbHVlXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHBvc2l0aW9uLnVuc2V0SW5kZXgob2xkVmFsdWUpO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBuZXdWYWx1ZSA9IHN0cmluZ2lmeShkZWx0YVtrZXldKTtcbiAgICAgIGNvbnN0IHBvc2l0aW9ucyA9IHZhbHVlc1tuZXdWYWx1ZV0gPSAodmFsdWVzW25ld1ZhbHVlXSB8fCBbXSk7XG4gICAgICBwb3NpdGlvbi5zZXRJbmRleChuZXdWYWx1ZSwgcG9zaXRpb25zLmxlbmd0aCk7XG4gICAgICBwb3NpdGlvbnMucHVzaChwb3NpdGlvbik7XG4gICAgfSk7XG4gIH1cblxuICByZW1vdmVQb3NpdGlvbihwb3NpdGlvbikge1xuICAgIHRoaXMuX2ZpbmQocG9zaXRpb24sICh2YWx1ZSwgcG9zaXRpb25zKSA9PiB7XG4gICAgICBjb25zdCBpbmRleCA9IHBvc2l0aW9uLmdldEluZGV4KHZhbHVlKTtcbiAgICAgIGlmIChpbmRleCA+IC0xKSB7XG4gICAgICAgIHBvc2l0aW9ucy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgfVxuICAgIH0sIHRydWUpO1xuICB9XG5cbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2xpYi9JbmRleC5qc1xuICoqLyIsIi8qKlxuICogU3RyaW5naWZ5IGFueSB2YWx1ZVxuICogQHBhcmFtIHsqfSB2YWx1ZVxuICogQHJldHVybnMge3N0cmluZ31cbiAqL1xuZXhwb3J0IGRlZmF1bHQgKHZhbHVlKSA9PiB7XG4gIGNvbnN0IHR5cGUgPSB0eXBlb2YgdmFsdWU7XG5cbiAgaWYgKHZhbHVlID09PSBudWxsKSB7XG4gICAgcmV0dXJuICdudWxsJztcbiAgfVxuXG4gIGlmICh0eXBlID09PSAndW5kZWZpbmVkJykge1xuICAgIHJldHVybiAndW5kZWZpbmVkJztcbiAgfVxuXG4gIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSB8fCB0eXBlID09PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBKU09OLnN0cmluZ2lmeSh2YWx1ZSk7XG4gIH1cblxuICByZXR1cm4gJycgKyB2YWx1ZTtcbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2xpYi9zdHJpbmdpZnkuanNcbiAqKi8iLCJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFF1ZXJ5IHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnJlc3VsdHMgPSBbXTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge1Bvc2l0aW9uW119IHBvc2l0aW9uc1xuICAgKi9cbiAgcHVzaChwb3NpdGlvbnMpIHtcbiAgICBwb3NpdGlvbnMuZm9yRWFjaChwb3NpdGlvbiA9PiB7XG4gICAgICBpZiAodGhpcy5yZXN1bHRzLmluZGV4T2YocG9zaXRpb24pID09PSAtMSkge1xuICAgICAgICB0aGlzLnJlc3VsdHMucHVzaChwb3NpdGlvbik7XG4gICAgICB9XG4gICAgfSwgdGhpcyk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBpc0VtcHR5KCkge1xuICAgIHJldHVybiAodGhpcy5zaXplKCkgPT09IDApO1xuICB9XG5cbiAgc2l6ZSgpIHtcbiAgICByZXR1cm4gdGhpcy5yZXN1bHRzLmxlbmd0aDtcbiAgfVxuXG4gIGF0KGluZGV4KSB7XG4gICAgY29uc3QgcG9zID0gdGhpcy5yZXN1bHRzW2luZGV4XTtcbiAgICByZXR1cm4gKHBvcyAmJiBwb3MuaW5zdGFuY2UpO1xuICB9XG5cbiAgZmlyc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMuYXQoMCk7XG4gIH1cblxuICBsYXN0KCkge1xuICAgIHJldHVybiB0aGlzLmF0KHRoaXMuc2l6ZSgpIC0gMSk7XG4gIH1cblxuICBhbGwoaXRlcmF0b3IpIHtcbiAgICBpZiAodHlwZW9mIGl0ZXJhdG9yID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICByZXR1cm4gdGhpcy5yZXN1bHRzLmZvckVhY2gocG9zID0+IGl0ZXJhdG9yKHBvcy5pbnN0YW5jZSkpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5yZXN1bHRzLm1hcChwb3MgPT4gcG9zLmluc3RhbmNlKTtcbiAgfVxuXG4gIHNvcnQoY29tcGFyYXRvcikge1xuICAgIHRoaXMucmVzdWx0cy5zb3J0KGNvbXBhcmF0b3IpO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2xpYi9RdWVyeS5qc1xuICoqLyIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIFNjaGVtYSB7XG5cbiAgLyoqXG4gICAqIEBzdGF0aWNcbiAgICogQHBhcmFtIHtvYmplY3R9IHByb3BzXG4gICAqIEByZXR1cm5zIHtvYmplY3R9XG4gICAqL1xuICBzdGF0aWMgZ2V0KHByb3BzKSB7XG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKHByb3BzKS5yZWR1Y2UoKHNjaGVtYSwga2V5KSA9PiB7XG4gICAgICBzY2hlbWFba2V5XSA9IFNjaGVtYS5kZWZpbmVWYWx1ZShwcm9wc1trZXldKTtcbiAgICAgIHJldHVybiBzY2hlbWE7XG4gICAgfSwge30pO1xuICB9O1xuXG4gIC8qKlxuICAgKiBAc3RhdGljXG4gICAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAgICogQHJldHVybnMgeyp9XG4gICAqL1xuICBzdGF0aWMgZGVmaW5lVmFsdWUodmFsdWUpIHtcbiAgICBjb25zdCB0eXBlID0gdHlwZW9mIHZhbHVlO1xuICAgIGlmICh0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICByZXR1cm4gbmV3IHZhbHVlKCk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGUgPT09ICdvYmplY3QnICYmIHZhbHVlIHx8IEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICByZXR1cm4gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh2YWx1ZSkpO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWU7XG4gIH1cblxufVxuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9saWIvU2NoZW1hLmpzXG4gKiovIiwiLyoqXG4gKiBAY2xhc3MgUG9zaXRpb25cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUG9zaXRpb24ge1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMubmV4dCA9IG51bGw7XG4gICAgdGhpcy5wcmV2aW91cyA9IG51bGw7XG4gICAgdGhpcy5pbnN0YW5jZSA9IG51bGw7XG4gICAgdGhpcy5maWVsZEluZGV4ID0ge307XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IGtleVxuICAgKiBAcmV0dXJucyB7Kn1cbiAgICovXG4gIGdldEluZGV4KGtleSkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkSW5kZXhba2V5XTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gICAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAgICovXG4gIHNldEluZGV4KGtleSwgdmFsdWUpIHtcbiAgICB0aGlzLmZpZWxkSW5kZXhba2V5XSA9IHZhbHVlO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBrZXlcbiAgICogQHJldHVybnMgeyp9XG4gICAqL1xuICB1bnNldEluZGV4KGtleSkge1xuICAgIGRlbGV0ZSB0aGlzLmZpZWxkSW5kZXhba2V5XTtcbiAgfVxuXG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9saWIvUG9zaXRpb24uanNcbiAqKi8iLCJpbXBvcnQgUmVzb3VyY2UgZnJvbSAnLi9SZXNvdXJjZSc7XG5cbmV4cG9ydCBjb25zdCBOQU1FU1BBQ0VTID0ge307XG5cbi8qKlxuICogQHB1YmxpY1xuICogQGNsYXNzIE5hbWVzcGFjZVxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOYW1lc3BhY2Uge1xuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZVxuICAgKiBAcmV0dXJucyB7TmFtZXNwYWNlfVxuICAgKi9cbiAgc3RhdGljIGNyZWF0ZShuYW1lKSB7XG4gICAgaWYgKE5BTUVTUEFDRVNbbmFtZV0pIHtcbiAgICAgIHJldHVybiBOQU1FU1BBQ0VTW25hbWVdO1xuICAgIH1cbiAgICByZXR1cm4gTkFNRVNQQUNFU1tuYW1lXSA9IG5ldyBOYW1lc3BhY2UobmFtZSk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAgICovXG4gIGNvbnN0cnVjdG9yKG5hbWUpIHtcbiAgICBpZiAoIW5hbWUpIHtcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1tuYW1lXSBtdXN0IGJlIGEgdmFsaWQgc3RyaW5nIScpO1xuICAgIH1cblxuICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgdGhpcy5rZXlzID0gbnVsbDtcbiAgICB0aGlzLnJlc291cmNlcyA9IHt9O1xuICAgIHRoaXMuaWRBdHRyaWJ1dGUgPSB1bmRlZmluZWQ7XG4gICAgdGhpcy5yZXNvdXJjZUJvZHkgPSBudWxsO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBvciBnZXQgYSBSZXNvdXJjZSBpbiBhIE5hbWVzcGFjZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gaWQgLSBpZCBhdHRyaWJ1dGVcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gY3RvciAtIGV4dGVuZGVkIFJlc291cmNlIGNvbnN0cnVjdG9yXG4gICAqIEByZXR1cm5zIHtSZXNvdXJjZX1cbiAgICovXG4gIHJlc291cmNlKG5hbWUsIGlkLCBjdG9yKSB7XG4gICAgY29uc3QgeyByZXNvdXJjZXMgfSA9IHRoaXM7XG4gICAgaWYgKHJlc291cmNlc1tuYW1lXSkge1xuICAgICAgcmV0dXJuIHJlc291cmNlc1tuYW1lXTtcbiAgICB9XG5cbiAgICBjdG9yID0gKGN0b3IgfHwgUmVzb3VyY2UpO1xuICAgIGlkID0gKGlkIHx8IHRoaXMuaWRBdHRyaWJ1dGUpO1xuXG4gICAgY29uc3QgcmVzb3VyY2UgPSBuZXcgY3RvcihuYW1lLCBpZCwgdGhpcyk7XG4gICAgY29uc3QgeyBrZXlzLCByZXNvdXJjZUJvZHkgfSA9IHRoaXM7XG5cbiAgICBpZiAoa2V5cykge1xuICAgICAgcmVzb3VyY2UuaW5kZXgoLi4ua2V5cyk7XG4gICAgfVxuXG4gICAgaWYgKHJlc291cmNlQm9keSkge1xuICAgICAgcmVzb3VyY2UuYm9keShyZXNvdXJjZUJvZHkpO1xuICAgIH1cblxuICAgIHJldHVybiByZXNvdXJjZXNbbmFtZV0gPSByZXNvdXJjZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIGRlZmF1bHQgaWRBdHRyaWJ1dGUgZm9yIFJlc291cmNlcyB3aXRoaW4gYSBOYW1lc3BhY2VcbiAgICogQHBhcmFtIHtzdHJpbmd9IGlkQXR0cmlidXRlXG4gICAqIEByZXR1cm5zIHtOYW1lc3BhY2V8c3RyaW5nfHVuZGVmaW5lZH1cbiAgICovXG4gIGlkKGlkQXR0cmlidXRlKSB7XG4gICAgaWYgKGlkQXR0cmlidXRlICYmICF0aGlzLmlkQXR0cmlidXRlKSB7XG4gICAgICB0aGlzLmlkQXR0cmlidXRlID0gaWRBdHRyaWJ1dGU7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMuaWRBdHRyaWJ1dGU7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFNldHMgZGVmYXVsdCBpbmRleCBrZXlzIGZvciBSZXNvdXJjZXMgd2l0aGluIGEgTmFtZXNwYWNlXG4gICAqIEBwYXJhbSB7c3RyaW5nW119IGtleXNcbiAgICogQHJldHVybnMge05hbWVzcGFjZX1cbiAgICovXG4gIGluZGV4KC4uLmtleXMpIHtcbiAgICB0aGlzLmtleXMgPSBrZXlzO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgLyoqXG4gICAqIFNldHMgZGVmYXVsdCBpZEF0dHJpYnV0ZSBmb3IgUmVzb3VyY2VzIHdpdGhpbiBhIE5hbWVzcGFjZVxuICAgKiBAcGFyYW0ge3N0cmluZ1tdfSBib2R5XG4gICAqIEByZXR1cm5zIHtOYW1lc3BhY2V8c3RyaW5nfHVuZGVmaW5lZH1cbiAgICovXG4gIGJvZHkoYm9keSkge1xuICAgIGlmIChib2R5ICYmICF0aGlzLnJlc291cmNlQm9keSkge1xuICAgICAgdGhpcy5yZXNvdXJjZUJvZHkgPSBib2R5O1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzLnJlc291cmNlQm9keTtcbiAgICB9XG4gIH1cblxufVxuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9saWIvTmFtZXNwYWNlLmpzXG4gKiovIl0sInNvdXJjZVJvb3QiOiIifQ==