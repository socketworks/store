((store) => {

  // example app namespace
  const namespace = store.namespace('org.app.example');
  namespace
    .id('_id')
    .body('body');

  // Documents resource
  const Documents = namespace.resource('documents');
  Documents.index('owner');
  // load some documents
  Documents.store({
    href: '/api/v1/documents/1/',
    meta: {
      reference: {
        owner: 'users'
      },
      type: 'documents'
    },
    body: {
      _id: 4,
      owner: 3,
      title: 'some title 1',
      content: 'some content 1'
    }
  });
  Documents.store({
    href: '/api/v1/documents/2/',
    meta: {
      reference: {
        owner: 'users'
      },
      type: 'documents'
    },
    body: {
      _id: 4,
      owner: 7,
      title: 'some title 2',
      content: 'some content 2'
    }
  });
  
  // Users resource
  const Users = namespace.resource('users');

  // MacBook Pro (Retina, 13-inch, Early 2015), OS X v10.11.3
  // 2.7 GHz Intel Core i5, 16 GB 1867 MHz DDR3

  // Users.index('name'); // find on 10K items: 26-28ms
  Users.index('name', 'age'); // find on 10K items: 4-5ms

  // load data into Users resource
  console.log('Loading users...');
  var i = 0;
  while (i < 10000) {
    Users.store({
      href: `/api/v1/users/${i}/`,
      meta: {
        references: { /* ... */ },
        type: 'users'
      },
      body: {
        _id: i,
        name: `user${i}`,
        age: (i < 3000 ? 30 : (i < 6000 ? 45 : 60))
      }
    });

    i++;
  }
  console.log('Users loaded!');

  console.log(`Available values to look up on age: ${Users.values('age')}`);
  console.log(`Available values to look up on name: ${Users.values('name')}`);
  var start = Date.now();
  Users.find('age', 45);

  console.log(Date.now() - start);

})(sktwrx_store);
