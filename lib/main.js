import Instance from './Instance';
import Resource from './Resource';
import Namespace from './Namespace';

import { CREATE, CHANGE, UPDATE, REMOVE, RESET } from './Evented';

export const namespace = (namespace) => Namespace.create(namespace);

export const resource = (name, idAttr) => new Resource(name, idAttr);

export default {
  Resource,
  Instance,
  Namespace,
  events: { CREATE, CHANGE, UPDATE, REMOVE, RESET }
};
