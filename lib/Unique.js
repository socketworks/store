import stringify from './stringify';

let ID = 0;

/**
 * Ensures that all instances have a unique identifier attribute Unique#_id
 * @class Unique
 */
export default class Unique {

  /**
   * Returns a prefixed unique id
   * @param {string} prefix
   * @returns {string}
   */
  static id(prefix = '_') {
    return stringify(prefix) + ID++;
  }
  
  /**
   * @param {string} prefix
   */
  constructor(prefix) {
    /**
     * @type {string}
     * @private
     */
    this._id = Unique.id(prefix);
  }

  /**
   * @returns {string}
   */
  getId() {
    return this._id;
  }
  
}
