import stringify from './stringify';

/**
 * @protected
 * @class Index
 */
export default class Index {

  constructor() {
    this.lookup = {};
    this.indexKeys = [];
  }

  /**
   * @private
   */
  _find(position, executor, deleteIfEmpty) {
    const body = position.instance.getProps();
    const { lookup } = this;

    this.indexKeys.forEach(key => {
      const value = stringify(body[key]);
      const values = lookup[key] = (lookup[key] || {});
      const positions = values[value] = (values[value] || []);

      executor(value, positions);

      if (deleteIfEmpty && positions.length === 0) {
        delete values[value];
      }
    });
  }

  indexKey(key) {
    if (this.indexKeys.indexOf(key) === -1) {
      this.indexKeys.push(key);
    }
  }

  getValuesFor(key) {
    return Object.keys(this.lookup[key] || {});
  }

  get(key, value) {
    const field = this.lookup[key];
    value = stringify(value);
    return (field && field[value] || null);
  }

  indexPosition(position) {
    this._find(position, (value, positions) => {
      position.setIndex(value, positions.length);
      positions.push(position);
    });
  }

  reindexPosition(position, oldValues, delta) {
    const { lookup } = this;

    this.indexKeys.forEach(key => {
      const values = lookup[key];
      const oldValue = stringify(oldValues[key]);

      if (values[oldValue]) {
        const index = position.getIndex(oldValue);
        values[oldValue].splice(index, 1);

        if (values[oldValue].length === 0) {
          delete values[oldValue];
        }

        position.unsetIndex(oldValue);
      }

      const newValue = stringify(delta[key]);
      const positions = values[newValue] = (values[newValue] || []);
      position.setIndex(newValue, positions.length);
      positions.push(position);
    });
  }

  removePosition(position) {
    this._find(position, (value, positions) => {
      const index = position.getIndex(value);
      if (index > -1) {
        positions.splice(index, 1);
      }
    }, true);
  }

};
