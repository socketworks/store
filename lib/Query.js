
export default class Query {

  constructor() {
    this.results = [];
  }

  /**
   * @param {Position[]} positions
   */
  push(positions) {
    positions.forEach(position => {
      if (this.results.indexOf(position) === -1) {
        this.results.push(position);
      }
    }, this);
    return this;
  }

  isEmpty() {
    return (this.size() === 0);
  }

  size() {
    return this.results.length;
  }

  at(index) {
    const pos = this.results[index];
    return (pos && pos.instance);
  }

  first() {
    return this.at(0);
  }

  last() {
    return this.at(this.size() - 1);
  }

  all(iterator) {
    if (typeof iterator === 'function') {
      return this.results.forEach(pos => iterator(pos.instance));
    }
    return this.results.map(pos => pos.instance);
  }

  sort(comparator) {
    this.results.sort(comparator);
    return this;
  }

};
