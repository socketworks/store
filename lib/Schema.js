export default class Schema {

  /**
   * @static
   * @param {object} props
   * @returns {object}
   */
  static get(props) {
    return Object.keys(props).reduce((schema, key) => {
      schema[key] = Schema.defineValue(props[key]);
      return schema;
    }, {});
  };

  /**
   * @static
   * @param {*} value
   * @returns {*}
   */
  static defineValue(value) {
    const type = typeof value;
    if (type === 'function') {
      return new value();
    }

    if (type === 'object' && value || Array.isArray(value)) {
      return JSON.parse(JSON.stringify(value));
    }
    return value;
  }

}
