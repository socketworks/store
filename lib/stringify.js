/**
 * Stringify any value
 * @param {*} value
 * @returns {string}
 */
export default (value) => {
  const type = typeof value;

  if (value === null) {
    return 'null';
  }

  if (type === 'undefined') {
    return 'undefined';
  }

  if (Array.isArray(value) || type === 'object') {
    return JSON.stringify(value);
  }

  return '' + value;
};
