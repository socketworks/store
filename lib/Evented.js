export const CREATE = 'create';
export const CHANGE = 'change';
export const UPDATE = 'update';
export const REMOVE = 'remove';
export const RESET = 'reset';

/**
 * @protected
 * @class Evented
 */
export default class Evented {

  constructor() {
    this.listeners = {};
  }

  /**
   * @param {string} type
   */
  registerEvent(type) {
    this.listeners[type] = (this.listeners[type] || []);
  }

  /**
   * @param {string[]} types
   */
  registerEvents(...types) {
    types.forEach(this.registerEvent, this);
  }

  /**
   * @param {string} type
   * @param {*[]} payload
   * @returns {Evented}
   */
  publish(type, ...payload) {
    if (!this.listeners[type]) {
      throw new Error(`${this.constructor.name} '${type}' is not a registered event!`);
    }
    this.listeners[type].forEach(listener => listener(...payload));
    return this;
  }

  /**
   * @param {string} type
   * @param {*[]} payload
   * @returns {Evented}
   */
  asyncPublish(type, ...payload) {
    setTimeout(() => this.publish(type, ...payload), 0);
    return this;
  }

  /**
   * @param {string} type
   * @param {function} listener
   * @returns {Evented}
   */
  subscribe(type, listener) {
    if (typeof listener !== 'function') {
      return;
    }

    const listeners = this.listeners[type];
    if (listeners && listeners.indexOf(listener) === -1) {
      listeners.push(listener);
    }
    return this;
  }

  /**
   * @param {string} type
   * @param {function} listener
   * @returns {Evented}
   */
  unsubscribe(type, listener) {
    const listeners = this.listeners[type];
    if (listeners) {
      if (typeof listener !== 'function') {
        this.listeners[type] = [];
      }

      const index = listeners.indexOf(listener);
      if (index > -1) {
        listeners.splice(index, 1);
      }
    }
    return this;
  }

  resetEvents() {
    const { listeners } = this;
    this.listeners = Object.keys(listeners).reduce((listeners, type) => {
      listeners[type] = [];
      return listeners;
    }, {});
  }

}
