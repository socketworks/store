import Index from './Index';
import Query from './Query';
import Schema from './Schema';
import Position from './Position';
import Instance from './Instance';
import Evented, { CREATE, CHANGE, UPDATE, REMOVE, RESET } from './Evented';

/**
 * @class Resource
 * @extends {Evented}
 */
export default class Resource extends Evented {

  constructor(name = '', idAttribute = 'id', namespace = null) {
    super(...arguments);

    this.idAttribute = idAttribute;
    this.namespace = namespace;
    this.name = name;
    this.order = [];
    this.resourceBody = null;
    this.Instance = Instance;
    this._props = {};
    this._schema = {};
    this._index = new Index(this);

    if (idAttribute) {
      this.index(idAttribute);
    }
    
    this.registerEvents(CREATE, CHANGE, UPDATE, REMOVE, RESET);
  }

  /**
   * @param {Resource} position
   * @param {object} oldProps all old props
   * @param {object} delta changed field with new values
     * @private
     */
  _reindex({ position }, oldProps, delta) {
    this._index.reindexPosition(position, oldProps, delta);
  }

  /**
   * @private
   * @param {object} props
   * @param {number} [index]
   * @returns {Instance|Instance[]}
   */
  _create(props, index) {
    if (typeof index !== 'number' || isNaN(index)) {
      index = this.order.length;
    }

    const position = new Position();
    const previous = (this.order[index - 1] || null);
    if (previous) {
      previous.next = position;
    }
    const next = (this.order[index] || null);
    if (next) {
      next.previous = position;
    }
    position.next = next;
    position.previous = previous;

    const schema = Schema.get(this._schema);
    const allProps = Object.assign({}, schema, props);
    const { Instance } = this;
    const instance = new Instance(this, allProps, position);
    position.instance = instance;

    this._index.indexPosition(position);
    this.order.splice(index, 0, position);

    instance.subscribe(UPDATE, (instance, oldProps, delta) => {
      this._index.reindexPosition(instance.position, oldProps, delta);
      this.publish(UPDATE, instance, oldProps, delta);
    });

    this.publish(CREATE, instance, index);

    return instance;
  }

  /**
   * @private
   * @param {object} props
   * @param {number} [index]
   * @returns {Instance|Instance[]}
   */
  _store(props, index) {
    const id = this.idAttribute;
    let query = this.find(id, props[id]);
    if (!query.isEmpty()) {
      const instance = query.first();
      instance.update(props);
      return instance;
    }

    return this._create(props, index);
  }

  /**
   * keys of instances to index
   * @param {string[]} keys
   * @returns {Resource}
     */
  index(...keys) {
    const { _index } = this;
    keys.forEach(_index.indexKey, _index);
    return this;
  }

  /**
   * Default keys with default values
   * @param {object} defaults
   * @returns {Resource}
   */
  defaults(defaults) {
    this._schema = defaults;
    return this;
  }

  /**
   * Set the Instance class
   * @param {Function} Instance
   */
  instance(Instance) {
    this.Instance = Instance;
  }

  /**
   * Specify the namespace to index for an Instance
   * This can be specified once only!
   * @param {string} key
   * @returns {*}
   */
  body(key) {
    if (key && !this.resourceBody) {
      this.resourceBody = key;
      return this;
    }
    return this.resourceBody;
  }

  get(key) {
    return this._props[key];
  }

  /**
   *
   * @param {string} key
   * @param {*} value
   * @returns {Resource}
   */
  set(key, value) {
    this.change({ [key]: value });
    return this;
  }

  /**
   * @param {object} props
   * @returns {Resource}
   */
  change(props) {
    const { _props } = this._props;
    props = Object.assign({}, _props, props);
    this._props = props;
    this.publish(CHANGE, props);
    return this;
  }

  /**
   * @param {object|object[]} props
   * @param {number} [index]
   * @returns {Resource|Resource[]}
   */
  store(props, index) {
    if (Array.isArray(props)) {
      return props.map((prop, i) => {
        const at  = (typeof index === 'number' ? index + i : undefined);
        return this._store(prop, at);
      });
    } else {
      return this._store(props, index);
    }
  }

  /**
   * Return positions for a key and a value
   * @param {string} key
   * @param {*} value
   * @returns {number[]|number}
   */
  position(key, value) {
    const positions = this._index.get(key, value);
    return (positions ? positions.map(pos => this.order.indexOf(pos)) : -1);
  }

  /**
   * @returns {Instance|null}
   */
  first() {
    return this.at(0);
  }

  /**
   * @returns {Instance|null}
   */
  last() {
    return this.at(this.order.length - 1);
  }

  /**
   *
   * @param {Instance} instance
   * @param {object} props
   * @returns {Resource}
   */
  before(instance, props) {
    const id = instance.id();
    const index = this.position(this.idAttribute, id)[0];
    return this.store(props, index);
  }
  
  /**
   *
   * @param {Instance} instance
   * @param {object} props
   * @returns {Resource}
   */
  after(instance, props) {
    const id = instance.id();
    const index = this.position(this.idAttribute, id)[0];
    return this.store(props, index + 1);
  }

  /**
   * @param {function} exec
   * @returns {array}
   */
  all(exec) {
    let iterator = pos => pos.instance;
    if (typeof exec === 'function') {
      iterator = (pos, ...args) => exec(pos.instance, ...args);
    }
    return this.order.map(iterator);
  }

  /**
   * @returns {number}
   */
  size() {
    return this.order.length;
  }

  /**
   * @param {string} field
   * @param {*} value
   * @param {query} [query]
   * @returns {Query}
   */
  find(field, value, query) {
    const positions = this._index.get(field, value);
    query = (query || new Query());

    if (positions) {
      return query.push(positions);
    }

    let position = this.order[0];
    while (position) {
      if (position.instance.get(field) === value) {
        query.push([position]);
      }
      position = position.next;
    }
    
    return query;
  }

  /**
   * @param {object} props
   */
  query(props) {
    const query = new Query();
    Object.keys(props).forEach(key => this.find(key, props[key], query));
    return query;
  }

  /**
   * @param {string} key
   * @returns {array}
   */
  values(key) {
    return this._index.getValuesFor(key);
  }

  /**
   * @param {number} index
   * @returns {Instance|null}
   */
  at(index) {
    const position = this.order[index];
    return (position && position.instance || null);
  }

  // convenience method only - use Collection#find/at -> Instance#update or Collection#upsert
  update(instance, updates) {
    instance.update(updates);
  }

  /**
   * @param {Instance} instance
   */
  remove(instance) {
    const { position } = instance;
    const index = this.order.indexOf(position);
    if (index > -1) {
      this.order.splice(index, 1);
    }

    this._index.removePosition(position);

    const { previous, next } = position;
    if (previous) {
      previous.next = next;
    }
    if (next) {
      next.previous = previous;
    }

    instance.resetEvents();
    this.publish(REMOVE, instance, index);
  }

  /**
   * @param {string} triggerRemoves
   */
  reset(triggerRemoves) {
    if (triggerRemoves === true) {
      this.order.forEach(({ instance }) => this.remove(instance));
    } else {
      this.order.forEach(pos => {
        pos.instance.resetEvents();
        this._index.removePosition(pos);
      });
    }
    this.order = [];
    this._props = {};
    this.publish(RESET);
  }

};
