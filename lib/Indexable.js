import Evented from './Evented';

/**
 * Indexable objects belong to a Position instance
 * @class Indexable
 * @extends {Evented}
 */
export default class Indexable extends Evented {

  /**
   * Throws an error if the passed argument is not an Indexable
   * @param {*} object
   * @throws {TypeError}
   */
  static ensure(object) {
    if (object instanceof Indexable) {
      return;
    }
    throw new TypeError('object is not an Indexable');
  }

  constructor() {
    super(...arguments);
    this.positionId = null;
  }

  /**
   * @returns {string}
   */
  getPositionId() {
    return this.positionId;
  }

  /**
   * @param {string} positionId
   */
  setPositionId(positionId) {
    this.positionId = positionId;
  }

}
