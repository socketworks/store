/**
 * @class Position
 */
export default class Position {

  constructor() {
    this.next = null;
    this.previous = null;
    this.instance = null;
    this.fieldIndex = {};
  }

  /**
   * @param {string} key
   * @returns {*}
   */
  getIndex(key) {
    return this.fieldIndex[key];
  }

  /**
   * @param {string} key
   * @param {*} value
   */
  setIndex(key, value) {
    this.fieldIndex[key] = value;
  }

  /**
   * @param {string} key
   * @returns {*}
   */
  unsetIndex(key) {
    delete this.fieldIndex[key];
  }

};
