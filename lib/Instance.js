import Evented, { CHANGE, UPDATE } from './Evented';

/**
 * @public
 * @class Instance
 * @extends {Evented}
 */
export default class Instance extends Evented {

  /**
   *
   * @param {Resource} resource
   * @param {object} props
   * @param {Position} position
     */
  constructor(resource, props = {}, position) {
    super(...arguments);

    this.resource = resource;
    this.position = position;
    this.props = props;
    this.changes = {};

    this.registerEvents(CHANGE, UPDATE);
  }

  getProps() {
    const body  = this.resource.body();
    if (body) {
      return this.props[body];
    }
    return this.props;
  }

  /**
   * Get next Instance in the chain
   * @returns {Instance|null}
   */
  next() {
    const next = this.position.next;
    return (next && next.instance || null);
  }

  /**
   * Get previous Instance in the chain
   * @returns {Instance|null}
   */
  previous() {
    const previous = this.position.previous;
    return (previous && previous.instance || null);
  }

  get(key) {
    const body = this.getProps();
    return body[key];
  }

  /**
   * Return the field specified by Resource#body()
   * @returns {string|null}
   */
  id() {
    const { idAttribute } = this.resource;
    if (idAttribute) {
      return this.get(idAttribute);
    }
    return null;
  }

  /**
   * Return the changed value to a field
   * @param {string} key
   * @returns {*}
   */
  getChange(key) {
    return this.changes[key];
  }

  /**
   * Update a field's value
   * @param {string} path
   *
   * @example
   *
   * Instance#resolve('a.b.c');
   *
   * @returns {*}
   */
  resolve(path = '') {
    path = (path && path.split('.') || []);
    let props = this.props;

    while (path.length > 0) {
      const field = path.shift();
      if (!props[field]) {
        return;
      }
      props = props[field];
    }

    return props;
  }

  /**
   * Update a body field's value
   * @param {string} field
   * @param {*} value
   * @returns {Instance}
   */
  set(field, value) {
    this.change({ [field]: value });
    return this;
  }

  /**
   * @param {object} changes
   */
  change(changes) {
    const oldChanges = this.changes;
    this.changes = Object.assign({}, oldChanges, changes);
    this.publish(CHANGE, this, oldChanges, changes);
  }

  /**
   * Update the resource Instance
   * @param {object} changes fields(s) - value(s)
   */
  update(changes) {
    const delta = Object.assign({}, this.changes, changes);
    const oldProps = this.getProps();
    const props = Object.assign({}, oldProps, delta);

    // if there are any actual changes
    if (JSON.stringify(props) !== JSON.stringify(this.props)) {
      this.changes = {};
      this.props = props;

      this.publish(UPDATE, this, oldProps, delta);
    }
  }

  remove() {
    this.resource.remove(this);
  }

  /**
   * @returns {object}
   */
  toJSON() {
    return JSON.parse(this.toString());
  }

  /**
   * @returns {string}
   */
  toString() {
    return JSON.stringify(this.props);
  }
};
