import Resource from './Resource';

export const NAMESPACES = {};

/**
 * @public
 * @class Namespace
 */
export default class Namespace {

  /**
   * @param {string} name
   * @returns {Namespace}
   */
  static create(name) {
    if (NAMESPACES[name]) {
      return NAMESPACES[name];
    }
    return NAMESPACES[name] = new Namespace(name);
  }

  /**
   * @param {string} name
   */
  constructor(name) {
    if (!name) {
      throw new TypeError('[name] must be a valid string!');
    }

    this.name = name;
    this.keys = null;
    this.resources = {};
    this.idAttribute = undefined;
    this.resourceBody = null;
  }

  /**
   * Create or get a Resource in a Namespace
   * @param {string} name
   * @param {string} id - id attribute
   * @param {Function} ctor - extended Resource constructor
   * @returns {Resource}
   */
  resource(name, id, ctor) {
    const { resources } = this;
    if (resources[name]) {
      return resources[name];
    }

    ctor = (ctor || Resource);
    id = (id || this.idAttribute);

    const resource = new ctor(name, id, this);
    const { keys, resourceBody } = this;

    if (keys) {
      resource.index(...keys);
    }

    if (resourceBody) {
      resource.body(resourceBody);
    }

    return resources[name] = resource;
  }

  /**
   * Sets default idAttribute for Resources within a Namespace
   * @param {string} idAttribute
   * @returns {Namespace|string|undefined}
   */
  id(idAttribute) {
    if (idAttribute && !this.idAttribute) {
      this.idAttribute = idAttribute;
      return this;
    } else {
      return this.idAttribute;
    }
  }

  /**
   * Sets default index keys for Resources within a Namespace
   * @param {string[]} keys
   * @returns {Namespace}
   */
  index(...keys) {
    this.keys = keys;
    return this;
  }

  /**
   * Sets default idAttribute for Resources within a Namespace
   * @param {string[]} body
   * @returns {Namespace|string|undefined}
   */
  body(body) {
    if (body && !this.resourceBody) {
      this.resourceBody = body;
      return this;
    } else {
      return this.resourceBody;
    }
  }

}
