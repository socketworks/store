const path = require('path');
const pkg = require('./package');

module.exports = {
  entry: path.join(__dirname, 'lib/main.js'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: `store.js`,
    library: `${pkg.name}`.replace('.', '_'),
    libraryTarget: 'umd'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      {
        loader: "babel-loader",
        test: /\.js$/,
        query: {
          //plugins: ['transform-runtime'],
          presets: ['es2015', 'stage-0']
        }
      }
    ]
  }

};
